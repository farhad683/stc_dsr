package com.farhad.sabtradecapital_deliveryman.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.farhad.sabtradecapital_deliveryman.R;
import com.farhad.sabtradecapital_deliveryman.models.invoice.Item;

import java.util.List;

public class DeliveredInvoiceAdapter extends RecyclerView.Adapter<DeliveredInvoiceAdapter.ReportViewHolder> {
    private OnItemClickListener listener;

    private List<Item> productItemList;

    public DeliveredInvoiceAdapter(List<Item> productItemList) {
        this.productItemList = productItemList;
    }

    @NonNull
    @Override
    public ReportViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.delivery_report_items_row, parent, false);
        return new ReportViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull ReportViewHolder holder, int position) {
        Item currentItem = productItemList.get(position);
        if(currentItem != null) {
            try {
                holder.productName.setText("" + currentItem.getProduct());
                holder.quantity.setText("" + currentItem.getQuantity());
                Double total = Double.parseDouble(currentItem.getTotal());
                holder.tvTotalAmount.setText("" + total.floatValue() + " \u09F3");
                holder.tvReturn.setText("" + currentItem.get_return() );
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public int getItemCount() {
        return productItemList.size();
    }

    public void changeTotalText(String text){

    }

    public class ReportViewHolder extends RecyclerView.ViewHolder{

        private TextView productName, quantity, tvTotalAmount, tvReturn;

        public ReportViewHolder(@NonNull View itemView){
            super(itemView);
            productName = itemView.findViewById(R.id.delivery_row_product_name);
            quantity = itemView.findViewById(R.id.delivery_row_quantity);
            tvTotalAmount = itemView.findViewById(R.id.delivery_row_total_amount);
            tvReturn = itemView.findViewById(R.id.delivery_row_rtn);

        }
    }

    public interface OnItemClickListener {
        void onItemClick(Item item, int position);
    }
    public void setOnItemClickListener(OnItemClickListener listener) {
        this.listener = listener;
    }
}
