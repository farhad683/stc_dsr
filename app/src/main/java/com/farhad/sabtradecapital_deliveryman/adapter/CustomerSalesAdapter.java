package com.farhad.sabtradecapital_deliveryman.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.RecyclerView;

import com.farhad.sabtradecapital_deliveryman.R;
import com.farhad.sabtradecapital_deliveryman.models.customer.Sale;
import com.farhad.sabtradecapital_deliveryman.ui.customer.CustomerDailyDeitalFragment;
import com.farhad.sabtradecapital_deliveryman.ui.delivery.DeliveryReportFragment;

import java.util.List;

public class CustomerSalesAdapter extends RecyclerView.Adapter<CustomerSalesAdapter.CustomerListVH> {
    private List<Sale> salesList;
    private Context mContext;

    public CustomerSalesAdapter(Context context, List<Sale> salesList) {
        this.salesList = salesList;
        mContext = context;
    }

    @NonNull
    @Override
    public CustomerListVH onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.cust_sales_row, parent, false);
        CustomerListVH clvh = new CustomerListVH(v);
        return clvh;
    }

    @Override
    public void onBindViewHolder(@NonNull final CustomerListVH holder, int position) {
        Sale currrentItem = salesList.get(position);
        if(currrentItem != null) {
            try {
                holder.cust_det_row_date.setText("" + currrentItem.getCreatedAt());
                holder.cust_det_row_total.setText("" + currrentItem.getTotalTp() + " \u09F3");
                holder.cust_det_row_paid.setText("" + currrentItem.getPaid() + " \u09F3");
                holder.cust_det_row_due.setText("" + (Double.parseDouble(currrentItem.getTotalTp()) - Double.parseDouble(currrentItem.getPaid())) + " \u09F3");


                holder.cust_det_row_action.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
//                  Toast.makeText(mContext, "Customer details will be shown", Toast.LENGTH_SHORT).show();
//                    Fragment newFragment = new CustomerDetailsFragment();
                        if(currrentItem.getDeliveryStatus().equals("delivered")){
                            Fragment fragment = DeliveryReportFragment.newInstance(currrentItem.getId());
                            AppCompatActivity activity = (AppCompatActivity) v.getContext();

                            FragmentManager fm = activity.getSupportFragmentManager();
                            fm.popBackStack();
                            FragmentTransaction transaction = fm
                                    .beginTransaction();


                            transaction.replace(R.id.nav_host_fragment, fragment);
                            transaction.addToBackStack(null);
                            transaction.commit();
                        } else {
                            Fragment fragment = CustomerDailyDeitalFragment.newInstance(currrentItem.getId());
                            AppCompatActivity activity = (AppCompatActivity) v.getContext();

                            FragmentManager fm = activity.getSupportFragmentManager();
                            fm.popBackStack();
                            FragmentTransaction transaction = fm
                                    .beginTransaction();


                            transaction.replace(R.id.nav_host_fragment, fragment);
                            transaction.addToBackStack(null);
                            transaction.commit();

                        }
                    }
                });
            } catch (Exception e){

            }
        }
    }

    @Override
    public int getItemCount() {
        return salesList.size();
    }

    public static class CustomerListVH extends RecyclerView.ViewHolder {
        public TextView cust_det_row_date, cust_det_row_total, cust_det_row_paid, cust_det_row_due;
        public ImageButton cust_det_row_action;

        public CustomerListVH(@NonNull View itemView) {
            super(itemView);
            cust_det_row_date = itemView.findViewById(R.id.cust_det_row_date);
            cust_det_row_total = itemView.findViewById(R.id.cust_det_row_total);
            cust_det_row_paid = itemView.findViewById(R.id.cust_det_row_paid);
            cust_det_row_due = itemView.findViewById(R.id.cust_det_row_due);
            cust_det_row_action = itemView.findViewById(R.id.cust_det_row_action);

        }
    }
}
