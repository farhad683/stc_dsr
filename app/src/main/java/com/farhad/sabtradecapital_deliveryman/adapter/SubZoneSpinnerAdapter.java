package com.farhad.sabtradecapital_deliveryman.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;


import com.farhad.sabtradecapital_deliveryman.R;
import com.farhad.sabtradecapital_deliveryman.models.subzone.SubZone;

import java.util.List;

public class SubZoneSpinnerAdapter extends ArrayAdapter<SubZone> {
    public SubZoneSpinnerAdapter(Context context, List<SubZone> zoneList) {
        super(context, 0, zoneList);
    }
    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        return initView(position, convertView, parent);
    }
    @Override
    public View getDropDownView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        return initView(position, convertView, parent);
    }
    private View initView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(
                    R.layout.area_spinner_row, parent, false
            );
        }
        TextView textViewName = convertView.findViewById(R.id.text_view_name);
        SubZone currentItem = getItem(position);
        if (currentItem != null) {
            textViewName.setText(currentItem.getSubZone());
        }
        return convertView;
    }
}