package com.farhad.sabtradecapital_deliveryman.adapter;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.RecyclerView;

import com.farhad.sabtradecapital_deliveryman.R;
import com.farhad.sabtradecapital_deliveryman.models.sales.Sales;
import com.farhad.sabtradecapital_deliveryman.ui.customer.CustomerDailyDeitalFragment;
import com.farhad.sabtradecapital_deliveryman.ui.order.OrderModel;

import java.util.List;

public class OrderListAdapter extends RecyclerView.Adapter<OrderListAdapter.OrderListVH> {
    private OnItemClickListener listener;

    private List<Sales> salesList;
    private Context mContext;

    public OrderListAdapter(Context context, List<Sales> salesList) {
        this.salesList = salesList;
        mContext = context;
    }

    @NonNull
    @Override
    public OrderListVH onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.order_list_row, parent, false);
        OrderListVH orderListVH = new OrderListVH(v);
        return orderListVH;
    }

    @Override
    public void onBindViewHolder(@NonNull OrderListVH holder, int position) {
        Sales currentItem = salesList.get(position);
        if (currentItem != null) {
            try {
                holder.txtViewID.setText("" + currentItem.getCustomerId());
                holder.txtViewDate.setText(currentItem.getCreatedAt());
                if (currentItem.getCustomer() != null) {
                    holder.txtViewShopName.setText("" + currentItem.getCustomer().getShop());
                    holder.txtViewAddress.setText("" + currentItem.getCustomer().getAddress());
                } else {
                    holder.txtViewShopName.setText("N/A");
                    holder.txtViewAddress.setText("N/A");
                }
                if (currentItem.getUser() != null) {
                    holder.txtSellBy.setText(currentItem.getUser().getName());
                } else {
                    holder.txtSellBy.setText("N/A");
                }
                holder.txtTotalAmount.setText("" + currentItem.getTotalTp() + " \u09F3");

                holder.order_list_status.setText("" + currentItem.getDeliveryStatus());
                /*if (currentItem.getDeliveryStatus().equals("pending")) {
                    holder.order_list_status.setText("pending");
                } else {
                    holder.order_list_status.setText("delivered");
                }*/
            } catch (Exception e) {

            }


            /*holder.tableLayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
//                Toast.makeText(mContext, "Invoice will be created.", Toast.LENGTH_SHORT).show();
//                Fragment newFragment = new CustomerDailyDeitalFragment();
                    CustomerDailyDeitalFragment newFragment = CustomerDailyDeitalFragment.newInstance(currentItem.getId());

                    AppCompatActivity activity = (AppCompatActivity) v.getContext();

                    FragmentManager fm = activity.getSupportFragmentManager();
                    fm.popBackStack();
                    FragmentTransaction transaction = fm
                            .beginTransaction();

                    transaction.replace(R.id.nav_host_fragment, newFragment);
                    transaction.addToBackStack(null);
                    transaction.commit();
//                    Toast.makeText(mContext, "" + currentItem.getCustomerId(), Toast.LENGTH_SHORT).show();
                }
            });*/
        }

    }

    @Override
    public int getItemCount() {
        return salesList.size();
    }

    public class OrderListVH extends RecyclerView.ViewHolder {
        public TextView txtViewID, txtViewDate, txtViewShopName, txtViewCustName, txtSellBy, txtTotalAmount, txtViewAddress;
        public TextView txtViewPrint;
        public CardView tableLayout;
        public TextView order_list_status;

        public OrderListVH(@NonNull View itemView) {

            super(itemView);

            txtViewID = itemView.findViewById(R.id.order_list_id);
            txtViewDate = itemView.findViewById(R.id.order_list_date);
            txtViewShopName = itemView.findViewById(R.id.order_list_shop_name);
            txtSellBy = itemView.findViewById(R.id.order_list_sell_by);
            txtTotalAmount = itemView.findViewById(R.id.order_list_total_amount);
            txtViewAddress = itemView.findViewById(R.id.order_list_address);

            order_list_status = itemView.findViewById(R.id.order_list_status);
            tableLayout = itemView.findViewById(R.id.table_row);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int position = getAdapterPosition();

                    if (listener != null && position != RecyclerView.NO_POSITION) {
                        listener.onItemClick(salesList.get(position));
                    }

//                    Fragment newFragment = new CustomerDailyDeitalFragment();
                    /*CustomerDailyDeitalFragment newFragment = CustomerDailyDeitalFragment.newInstance(salesList.get(position).getId());

                    AppCompatActivity activity = (AppCompatActivity) v.getContext();

                    FragmentManager fm = activity.getSupportFragmentManager();
                    fm.popBackStack();
                    FragmentTransaction transaction = fm
                            .beginTransaction();

                    transaction.replace(R.id.nav_host_fragment, newFragment);
                    transaction.addToBackStack(null);
                    transaction.commit();*/
                }
            });

        }
    }
    public interface OnItemClickListener {
        void onItemClick(Sales note);
    }
    public void setOnItemClickListener(OnItemClickListener listener) {
        this.listener = listener;
    }

}
