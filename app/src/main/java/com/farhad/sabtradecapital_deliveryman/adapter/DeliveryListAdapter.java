package com.farhad.sabtradecapital_deliveryman.adapter;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.RecyclerView;

import com.farhad.sabtradecapital_deliveryman.R;
import com.farhad.sabtradecapital_deliveryman.models.DeliveryModel;
import com.farhad.sabtradecapital_deliveryman.ui.customer.CustomerDailyDeitalFragment;
import com.farhad.sabtradecapital_deliveryman.ui.order.OrderModel;

import java.util.ArrayList;

public class DeliveryListAdapter extends RecyclerView.Adapter<DeliveryListAdapter.DeliveryListVH> {
    private ArrayList<DeliveryModel> deliveryList;
    private Context mContext;

    public DeliveryListAdapter(Context context, ArrayList<DeliveryModel> deliveryList) {
        this.deliveryList = deliveryList;
        mContext = context;
    }

    @NonNull
    @Override
    public DeliveryListVH onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.delivery_list_row, parent, false);
        DeliveryListVH orderListVH = new DeliveryListVH(v);
        return orderListVH;
    }

    @Override
    public void onBindViewHolder(@NonNull DeliveryListVH holder, int position) {
        DeliveryModel currentItem = deliveryList.get(position);
        holder.txtViewID.setText("" + currentItem.getId());
        holder.txtViewDate.setText(currentItem.getDate());
        holder.txtViewShopName.setText(currentItem.getShopName());
        holder.txtSellBy.setText(currentItem.getSellBy());
        holder.txtTotalAmount.setText("" + currentItem.getTotalAmount());
        holder.txtViewAddress.setText("" + currentItem.getAddress());
        holder.dueTextView.setText("" + currentItem.getTotalDue());
        holder.order_list_details.setText("" + currentItem.getStatus());

        if(position % 2 == 1 ){
            holder.tableLayout.setBackgroundColor(Color.LTGRAY);
        }
        if(currentItem.getStatus().equals("Delivered") ){
            holder.tableLayout.setBackgroundColor(Color.RED);
            holder.order_list_details.setEnabled(false);
            holder.txtViewDate.setTextColor(Color.WHITE);
            holder.txtViewShopName.setTextColor(Color.WHITE);
            holder.txtTotalAmount.setTextColor(Color.WHITE);
            holder.dueTextView.setTextColor(Color.WHITE);

        }

    }

    @Override
    public int getItemCount() {
        return deliveryList.size();
    }

    public static class DeliveryListVH extends RecyclerView.ViewHolder{
        public TextView txtViewID, txtViewDate, txtViewShopName, txtViewCustName,txtSellBy, txtTotalAmount, txtViewAddress;
        public TextView order_list_details;
        public TextView txtViewPrint, dueTextView;
        public CardView tableLayout;

        public DeliveryListVH(@NonNull View itemView) {

            super(itemView);

            txtViewID = itemView.findViewById(R.id.delivery_order_list_id);
            txtViewDate = itemView.findViewById(R.id.delivery_list_date);
            txtViewShopName = itemView.findViewById(R.id.delivery_list_shop_name);
            txtSellBy = itemView.findViewById(R.id.delivery_list_sell_by);
            txtTotalAmount = itemView.findViewById(R.id.delivery_list_total_amount);
            txtViewAddress = itemView.findViewById(R.id.delivery_list_address);
            dueTextView = itemView.findViewById(R.id.delivery_list_due_amount);

            order_list_details = itemView.findViewById(R.id.delivery_list_details);
            tableLayout = itemView.findViewById(R.id.table_row);

        }
    }

}
