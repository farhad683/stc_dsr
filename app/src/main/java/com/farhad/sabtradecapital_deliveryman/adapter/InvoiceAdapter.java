package com.farhad.sabtradecapital_deliveryman.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.farhad.sabtradecapital_deliveryman.R;
import com.farhad.sabtradecapital_deliveryman.models.invoice.Item;

import java.util.List;

public class InvoiceAdapter extends RecyclerView.Adapter<InvoiceAdapter.ReportViewHolder> {
    private OnItemClickListener listener;

    private List<Item> productItemList;

    public InvoiceAdapter(List<Item> productItemList) {
        this.productItemList = productItemList;
    }

    @NonNull
    @Override
    public ReportViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.invoice_items_row, parent, false);
        return new ReportViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull ReportViewHolder holder, int position) {
        Item currentItem = productItemList.get(position);
        if(currentItem != null) {
            try {
                holder.productName.setText("" + currentItem.getProduct());
                holder.quantity.setText("" + currentItem.getQuantity());
                Double tot = Double.parseDouble(currentItem.getTotal());
                holder.tvTotalAmount.setText("" + tot.floatValue() + " \u09F3");
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public int getItemCount() {
        return productItemList.size();
    }

    public void changeTotalText(String text){

    }

    public class ReportViewHolder extends RecyclerView.ViewHolder{

        private TextView productName, quantity, tvTotalAmount;
        private Button returnValue;

        public ReportViewHolder(@NonNull View itemView){
            super(itemView);
            productName = itemView.findViewById(R.id.invoice_product_name);
            quantity = itemView.findViewById(R.id.invoice_quantity);
            tvTotalAmount = itemView.findViewById(R.id.invoice_total_amount);
            returnValue = itemView.findViewById(R.id.invoice_return);

            returnValue.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int position = getAdapterPosition();
                    if(listener != null && position != RecyclerView.NO_POSITION){
                        listener.onItemClick(productItemList.get(position), position);
                    }
                }
            });
        }
    }

    public interface OnItemClickListener {
        void onItemClick(Item item, int position);
    }
    public void setOnItemClickListener(OnItemClickListener listener) {
        this.listener = listener;
    }
}
