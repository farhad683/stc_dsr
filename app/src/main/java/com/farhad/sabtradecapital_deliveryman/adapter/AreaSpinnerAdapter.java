package com.farhad.sabtradecapital_deliveryman.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.farhad.sabtradecapital_deliveryman.R;
import com.farhad.sabtradecapital_deliveryman.models.area_customer.Data;

import java.util.List;

public class AreaSpinnerAdapter extends ArrayAdapter<Data> {
    public AreaSpinnerAdapter(Context context, List<Data> areaList) {
        super(context, 0, areaList);
    }
    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        return initView(position, convertView, parent);
    }
    @Override
    public View getDropDownView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        return initView(position, convertView, parent);
    }
    private View initView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(
                    R.layout.area_spinner_row, parent, false
            );
        }
        TextView textViewName = convertView.findViewById(R.id.text_view_name);
        Data currentItem = getItem(position);
        if (currentItem != null) {
            textViewName.setText(currentItem.getArea());
        }
        return convertView;
    }
}