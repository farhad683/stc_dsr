package com.farhad.sabtradecapital_deliveryman.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.RecyclerView;

import com.farhad.sabtradecapital_deliveryman.R;
import com.farhad.sabtradecapital_deliveryman.customer.CustomerItem;
import com.farhad.sabtradecapital_deliveryman.models.sales_order.Customer;
import com.farhad.sabtradecapital_deliveryman.ui.TicketFragment;
import com.farhad.sabtradecapital_deliveryman.ui.customer.CustomerDetailsFragment;

import java.util.ArrayList;
import java.util.List;

public class CustomerListAdapter extends RecyclerView.Adapter<CustomerListAdapter.CustomerListVH> {
    private List<Customer> customerList;
    private Context mContext;

    public CustomerListAdapter(Context context, List<Customer> customerList) {
        this.customerList = customerList;
        mContext = context;
    }

    @NonNull
    @Override
    public CustomerListVH onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.customer_list_row, parent, false);
        CustomerListVH clvh = new CustomerListVH(v);
        return clvh;
    }

    @Override
    public void onBindViewHolder(@NonNull final CustomerListVH holder, int position) {
        Customer currrentItem = customerList.get(position);
        if(currrentItem != null) {
            holder.txtViewID.setText("" + currrentItem.getId());
            holder.txtViewShopName.setText("" + currrentItem.getShop());
            holder.txtViewMobile.setText("" + currrentItem.getName());
            holder.txtViewAddedBy.setText("" + Float.parseFloat(currrentItem.getDue()));

            holder.btnDetails.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
//                Toast.makeText(mContext, "Customer details will be shown", Toast.LENGTH_SHORT).show();
//                    Fragment newFragment = new CustomerDetailsFragment();
                    Fragment fragment = CustomerDetailsFragment.newInstance(currrentItem.getId());
                    AppCompatActivity activity = (AppCompatActivity) v.getContext();

                    FragmentManager fm = activity.getSupportFragmentManager();
                    fm.popBackStack();
                    FragmentTransaction transaction = fm
                            .beginTransaction();


                    transaction.replace(R.id.nav_host_fragment, fragment);
                    transaction.addToBackStack(null);
                    transaction.commit();

                }
            });
        }
    }

    @Override
    public int getItemCount() {
        return customerList.size();
    }

    public static class CustomerListVH extends RecyclerView.ViewHolder {
        public TextView txtViewID, txtViewShopName, txtViewMobile, txtViewAddedBy;
        public ImageButton btnDetails;

        public CustomerListVH(@NonNull View itemView) {
            super(itemView);
            txtViewID = itemView.findViewById(R.id.customer_list_id);
            txtViewShopName = itemView.findViewById(R.id.customer_list_shop_name);
            txtViewMobile = itemView.findViewById(R.id.customer_list_mobile);
            txtViewAddedBy = itemView.findViewById(R.id.customer_list_added_by);
            btnDetails = itemView.findViewById(R.id.show_customer_details);

        }
    }
}
