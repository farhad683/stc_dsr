package com.farhad.sabtradecapital_deliveryman.db;

import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity(tableName = "access")
public class AccessKey {
    @PrimaryKey(autoGenerate = true)
    private int id;

    private String access_key;

    public AccessKey(String access_key) {
        this.access_key = access_key;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getAccess_key() {
        return access_key;
    }

    public void setAccess_key(String access_key) {
        this.access_key = access_key;
    }
}
