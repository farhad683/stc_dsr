package com.farhad.sabtradecapital_deliveryman.db;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;


import com.farhad.sabtradecapital_deliveryman.models.Cart;

import java.util.List;

@Dao
public interface SabtradeDao {
    @Insert
    void insert(AccessKey key);

    @Query("SELECT * FROM access WHERE id = 1")
    LiveData<AccessKey> getKey();

    @Query("SELECT * FROM cart_table")
    LiveData<List<Cart>> getCarts();

    @Update
    void updateCart(Cart cart);

    @Delete
    void deleteCart(Cart cart);

    @Query("DELETE FROM cart_table")
    void deleteAllCart();



    @Insert
    void insert(Cart cart);


}
