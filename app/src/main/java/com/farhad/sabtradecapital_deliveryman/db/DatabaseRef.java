package com.farhad.sabtradecapital_deliveryman.db;

import android.content.Context;

import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;

import com.farhad.sabtradecapital_deliveryman.models.Cart;


@Database(entities = {AccessKey.class, Cart.class}, version = 1)
public abstract class DatabaseRef extends RoomDatabase {
    private static DatabaseRef instance;
    public abstract SabtradeDao dao();

    public static synchronized DatabaseRef getInstance(Context context) {
        if (instance == null) {
            instance = Room.databaseBuilder(context.getApplicationContext(),
                    DatabaseRef.class, "deliveryman_db")
                    .fallbackToDestructiveMigration()
                    .allowMainThreadQueries()
                    .build();
        }
        return instance;
    }
}
