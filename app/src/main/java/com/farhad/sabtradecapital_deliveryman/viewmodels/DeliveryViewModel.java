package com.farhad.sabtradecapital_deliveryman.viewmodels;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import com.farhad.sabtradecapital_deliveryman.models.delivery.DeliveryData;
import com.farhad.sabtradecapital_deliveryman.models.delivery.DeliveryResponse;
import com.farhad.sabtradecapital_deliveryman.models.invoice.Invoice;
import com.farhad.sabtradecapital_deliveryman.repo.DeliveryRepository;
import com.farhad.sabtradecapital_deliveryman.repo.InvoiceRepository;

public class DeliveryViewModel extends AndroidViewModel {
    private DeliveryRepository deliveryRepository;

    public DeliveryViewModel(@NonNull Application application) {
        super(application);
        deliveryRepository = new DeliveryRepository(application);
    }

    public LiveData<DeliveryResponse> confirmDelivery(int id, DeliveryData deliveryData){
        return deliveryRepository.confirmDelivery(id, deliveryData);
    }
}
