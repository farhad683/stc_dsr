package com.farhad.sabtradecapital_deliveryman.viewmodels;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import com.farhad.sabtradecapital_deliveryman.models.stock.StockData;
import com.farhad.sabtradecapital_deliveryman.repo.ProductRepository;

public class StockViewModel extends AndroidViewModel {
    private ProductRepository productRepository;

    public StockViewModel(@NonNull Application application) {
        super(application);
        productRepository = new ProductRepository(application);
    }

    public LiveData<StockData> getStocks(){
        return productRepository.getStocks();
    }

}
