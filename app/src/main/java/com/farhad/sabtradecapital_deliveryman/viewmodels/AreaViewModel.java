package com.farhad.sabtradecapital_deliveryman.viewmodels;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import com.farhad.sabtradecapital_deliveryman.models.area.Area;
import com.farhad.sabtradecapital_deliveryman.models.area_customer.Data;
import com.farhad.sabtradecapital_deliveryman.models.zone.Zone;
import com.farhad.sabtradecapital_deliveryman.repo.AreaRepository;
import com.farhad.sabtradecapital_deliveryman.repo.ZoneRepository;

import java.util.List;

public class AreaViewModel extends AndroidViewModel {
    private AreaRepository areaRepository;
    private ZoneRepository zoneRepository;

    public AreaViewModel(@NonNull Application application) {
        super(application);
        areaRepository = new AreaRepository(application);
        zoneRepository = new ZoneRepository(application);
    }

    public LiveData<List<Area>> getAllAreas(){
        return areaRepository.getAllAreas();
    }
    public LiveData<List<Zone>> getZoneFromArea(int id){
        return zoneRepository.getAllZoneFromArea(id);
    }
    public LiveData<List<Data>> getAreasForSelect(){
        return areaRepository.getAreasForSelect();
    }
}
