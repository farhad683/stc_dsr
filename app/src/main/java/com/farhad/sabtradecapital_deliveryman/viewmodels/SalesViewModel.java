package com.farhad.sabtradecapital_deliveryman.viewmodels;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import com.farhad.sabtradecapital_deliveryman.models.sales.Sales;
import com.farhad.sabtradecapital_deliveryman.models.sales_create.SalesCreate;
import com.farhad.sabtradecapital_deliveryman.models.sales_order.CreateSales;
import com.farhad.sabtradecapital_deliveryman.models.sales_order.SalesResponse;
import com.farhad.sabtradecapital_deliveryman.repo.SalesRepository;

import java.util.List;

public class SalesViewModel extends AndroidViewModel {
    private SalesRepository salesRepository;

    public SalesViewModel(@NonNull Application application) {
        super(application);
        salesRepository = new SalesRepository(application);
    }

    public LiveData<List<Sales>> getAllSales(){
        return salesRepository.getSales();
    }
    public LiveData<List<Sales>> getTodaysSales(){
        return salesRepository.getTodaysSales();
    }
    public LiveData<SalesResponse> createSales(CreateSales createSales){
        return salesRepository.createSales(createSales);
    }
    public LiveData<SalesCreate> getSalesCreate(){
        return salesRepository.getSalesCreate();
    }
}
