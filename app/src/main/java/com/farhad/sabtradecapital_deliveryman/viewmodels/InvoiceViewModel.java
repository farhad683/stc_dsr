package com.farhad.sabtradecapital_deliveryman.viewmodels;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import com.farhad.sabtradecapital_deliveryman.models.invoice.Invoice;
import com.farhad.sabtradecapital_deliveryman.repo.InvoiceRepository;

public class InvoiceViewModel extends AndroidViewModel {
    private InvoiceRepository invoiceRepository;

    public InvoiceViewModel(@NonNull Application application) {
        super(application);
        invoiceRepository = new InvoiceRepository(application);
    }

    public LiveData<Invoice> getSales(int id){
        return invoiceRepository.getSales(id);
    }
}
