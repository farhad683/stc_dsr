package com.farhad.sabtradecapital_deliveryman.viewmodels;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import com.farhad.sabtradecapital_deliveryman.models.category.Category;
import com.farhad.sabtradecapital_deliveryman.models.product.Product;
import com.farhad.sabtradecapital_deliveryman.repo.ProductRepository;

import java.util.List;

public class ProductViewModel extends AndroidViewModel {
    private ProductRepository productRepository;

    public ProductViewModel(@NonNull Application application) {
        super(application);
        productRepository = new ProductRepository(application);
    }

    public LiveData<List<Category>> getCategories(){
        return productRepository.getAllCategories();
    }
    public LiveData<List<Product>> getProductFromCategory(int catId){
        return productRepository.getProductsFromCategory(catId);
    }
}
