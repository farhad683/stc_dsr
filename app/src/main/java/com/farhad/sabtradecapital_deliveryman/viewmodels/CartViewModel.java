package com.farhad.sabtradecapital_deliveryman.viewmodels;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import com.farhad.sabtradecapital_deliveryman.models.Cart;
import com.farhad.sabtradecapital_deliveryman.repo.CartRepository;

import java.util.List;

public class CartViewModel extends AndroidViewModel {
    private CartRepository cartRepository;
    private LiveData<List<Cart>> allCarts;

    public CartViewModel(@NonNull Application application) {
        super(application);
        cartRepository = new CartRepository(application);
        allCarts = cartRepository.getAllCarts();
    }

    public void insertCart(Cart cart){
        cartRepository.insertCart(cart);
    }

    public void updateCart(Cart cart){
        cartRepository.updateCart(cart);
    }
    public void deleteCart(Cart cart){
        cartRepository.deleteCart(cart);
    }

    public void deleteAllCart(){
        cartRepository.deleteAllCart();
    }

    public LiveData<List<Cart>> getAllCarts(){
        return allCarts;
    }
}
