package com.farhad.sabtradecapital_deliveryman.viewmodels;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import com.farhad.sabtradecapital_deliveryman.models.customer.Customer;
import com.farhad.sabtradecapital_deliveryman.models.customer.Data;
import com.farhad.sabtradecapital_deliveryman.repo.CustomerRepository;

import java.util.List;

public class CustomerViewModel extends AndroidViewModel {

    private CustomerRepository customerRepository;

    public CustomerViewModel(@NonNull Application application) {
        super(application);
        customerRepository = new CustomerRepository(application);
    }

    public LiveData<Data> getCustomer(int id){
        return customerRepository.getCustomerData(id);
    }

    public LiveData<List<Customer>> getAllCustomers(){
        return customerRepository.getAllCustomers();
    }
    public LiveData<List<com.farhad.sabtradecapital_deliveryman.models.sales_order.Customer>> getAllCustomersFromZone(int id){
        return customerRepository.getAllCustomerFromZone(id);
    }

    public LiveData<com.farhad.sabtradecapital_deliveryman.models.Customer> addCustomer(com.farhad.sabtradecapital_deliveryman.models.Customer customer){
        return customerRepository.addCustomer(customer);
    }
}
