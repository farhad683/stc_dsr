package com.farhad.sabtradecapital_deliveryman.api;

import com.farhad.sabtradecapital_deliveryman.models.Cart;
import com.farhad.sabtradecapital_deliveryman.models.Customer;
import com.farhad.sabtradecapital_deliveryman.models.Datum;
import com.farhad.sabtradecapital_deliveryman.models.User;
import com.farhad.sabtradecapital_deliveryman.models.area.AreaData;
import com.farhad.sabtradecapital_deliveryman.models.area_customer.Area;
import com.farhad.sabtradecapital_deliveryman.models.category.CategoryData;
import com.farhad.sabtradecapital_deliveryman.models.customer.CustomerData;
import com.farhad.sabtradecapital_deliveryman.models.customer.CustomerResponse;
import com.farhad.sabtradecapital_deliveryman.models.delivery.DeliveryData;
import com.farhad.sabtradecapital_deliveryman.models.delivery.DeliveryResponse;
import com.farhad.sabtradecapital_deliveryman.models.delivery.DiscountData;
import com.farhad.sabtradecapital_deliveryman.models.delivery.DiscountModel;
import com.farhad.sabtradecapital_deliveryman.models.product.ProductData;
import com.farhad.sabtradecapital_deliveryman.models.sales.Data;
import com.farhad.sabtradecapital_deliveryman.models.sales_create.SalesCreate;
import com.farhad.sabtradecapital_deliveryman.models.sales_order.CreateSales;
import com.farhad.sabtradecapital_deliveryman.models.sales_order.CustomerFromZone;
import com.farhad.sabtradecapital_deliveryman.models.sales_order.SalesResponse;
import com.farhad.sabtradecapital_deliveryman.models.stock.StockData;
import com.farhad.sabtradecapital_deliveryman.models.subzone.SubZoneData;
import com.farhad.sabtradecapital_deliveryman.models.zone.ZoneData;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.PATCH;
import retrofit2.http.POST;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface ApiServices {

    @POST("auth/login")
    Call<User> login(@Query("email") String email, @Query("password") String password);



    //// Customer-------------
    @GET("customers/{id}")
    Call<CustomerResponse> getCustomer(@Path("id") int id);

    @GET("customers")
    Call<CustomerData> getCustomers();

    @GET("customers/fromZone/{zoneId}")
    Call<CustomerFromZone> getCustomersFromZone(@Path("zoneId") int zoneId);

    @POST("customers")
    Call<Customer> addCustomer(@Body Customer customer);


    @GET("customers")
    Call<Datum> getAllCustomer();

    @GET("customers/fromSubZone/{subZoneId}")
    Call<CustomerFromZone> getSubZoneCustomer(@Path("subZoneId") int subZoneId);

    /// Area
    @GET("areas")
    Call<AreaData> getAreas();

    @GET("areas/getAreaForSelect")
    Call<Area> getAreasForSelect();

    /// Zone
    @GET("zones/fromArea/{area_id}")
    Call<ZoneData> getZoneFromArea(@Path("area_id") int areaId);

    @GET("sub_zones/fromZone/{zone_id}")
    Call<SubZoneData> getSubZoneFromZone(@Path("zone_id") int zone_id);

    //// Sales -------------------------------

    @POST("sales")
    Call<SalesResponse> createSales(@Body CreateSales createSales);

    @GET("sales")
    Call<Data> getSales();

    @GET("sales/create")
    Call<SalesCreate> getSalesCreate();

    @GET("sales/today_orders")
    Call<Data> getTodaysSales();

    @GET("sales/{id}")
    Call<com.farhad.sabtradecapital_deliveryman.models.invoice.Data> getSales(@Path("id") int id);

    //// Product
    @GET("categories")
    Call<CategoryData> getCategories();

    @GET("product/fromCategoryWithPrice/{category}")
    Call<ProductData> getProductFromCategory(@Path("category") int category);

    @POST("sales/{id}/discount")
    Call<DiscountData> discount(@Path("id") int id, @Body DiscountModel discountModel);

    // Stock
    @GET("stocks")
    Call<StockData> getStocks();

    // Delivery
    @PATCH("sales/{id}")
    Call<DeliveryResponse> confirmDelivery(@Path("id") int id, @Body DeliveryData deliveryData);

}
