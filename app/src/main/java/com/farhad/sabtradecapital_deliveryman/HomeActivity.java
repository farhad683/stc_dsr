package com.farhad.sabtradecapital_deliveryman;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.Menu;
import android.widget.FrameLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.farhad.sabtradecapital_deliveryman.api.ApiRef;
import com.farhad.sabtradecapital_deliveryman.api.ApiServices;
import com.farhad.sabtradecapital_deliveryman.models.User;
import com.farhad.sabtradecapital_deliveryman.ui.delivery.DeliveryFragment;
import com.farhad.sabtradecapital_deliveryman.ui.customer.CustomerFragment;
import com.farhad.sabtradecapital_deliveryman.ui.delivery.PendingOrderFragment;
import com.farhad.sabtradecapital_deliveryman.ui.home.HomeFragment;
import com.farhad.sabtradecapital_deliveryman.ui.order.OrderListFragment;
import com.farhad.sabtradecapital_deliveryman.ui.order.TodayOrdersFragment;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;
import com.google.android.material.navigation.NavigationView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.core.view.GravityCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.navigation.ui.AppBarConfiguration;
import androidx.navigation.ui.NavigationUI;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.farhad.sabtradecapital_deliveryman.LoginActivity.SHARED_PREFS;

public class HomeActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {
    private static final int HOME_FRAGMENT = 0;
    private static final int ORDER_LIST_FRAGMENT = 1;
    private static final int TOTAL_ORDER_FRAGMENT = 2;
    private static final int DELIVERY_REPORT = 3;
    private static final int CUSTOMER_REPORT_FRAGMENT = 4;
    private static final int CUSTOMER_DETAILS_FRAGMENT = 5;
    private static final int PENDING_ORDER_FRAGMENT = 6;

    private int currentFragment = -1;
    

    private AppBarConfiguration mAppBarConfiguration;
    private BottomNavigationView bottomNav;
    
    private DrawerLayout drawer;
    private NavigationView navigationView;
    private FrameLayout frameLayout;

    private TextView navHeaderName, navHeaderEmail;

    private SharedPreferences sharedPreferences;
    private ApiServices apiServices;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        toolbar.setTitle("DSR - " + getUserName());

        FloatingActionButton fab = findViewById(R.id.fab);
        sharedPreferences = getSharedPreferences(SHARED_PREFS, MODE_PRIVATE);
        apiServices = ApiRef.getInstance().create(ApiServices.class);

        login();
        Log.d("TAG", "onCreate: HomeActivity");

        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });
        drawer = findViewById(R.id.drawer_layout);
        navigationView = findViewById(R.id.nav_view);

        /// navigation header
        View headerView = navigationView.getHeaderView(0);
        navHeaderName = headerView.findViewById(R.id.nav_header_name);
        navHeaderEmail = headerView.findViewById(R.id.nav_header_email);
        navHeaderName.setText(getUserName());
        navHeaderEmail.setText(getUserEmail());

        
        navigationView.setNavigationItemSelectedListener(this);
        navigationView.getMenu().getItem(0).setChecked(true);
        

        bottomNav = findViewById(R.id.bottom_navigation);
        bottomNav.setOnNavigationItemSelectedListener(navListener);
        
        frameLayout = findViewById(R.id.nav_host_fragment);

        //I added this if statement to keep the selected fragment when rotating the device
        if (savedInstanceState == null) {
            getSupportFragmentManager().beginTransaction().replace(R.id.nav_host_fragment,
                    new HomeFragment()).commit();
        }

        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();
        setFragment(new HomeFragment(), HOME_FRAGMENT);
    }

    private void login() {
        apiServices.login(getUserEmail(), getPassword()).enqueue(new Callback<User>() {
            @Override
            public void onResponse(Call<User> call, Response<User> response) {
                if(response.isSuccessful()){
                    if(response.code() == 200){
                        if(response.body() != null) {
                            saveData(response.body().getData().getAccessToken(), response.body().getData().getUser().getName(), response.body().getData().getUser().getEmail());
//                        startActivity(new Intent(LoginActivity.this, HomeActivity.class));
//                        loadingDialog.dismiss();
//                        finish();
                        } else {
                            logout();
                        }
                    }
                } else {
                    logout();
                    Toast.makeText(HomeActivity.this, "Invalid email or password", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<User> call, Throwable t) {
                Log.d("TAG", "onResponse: " + t.getMessage());
                Log.d("TAG", "onResponse: " + t.getMessage());
//                Toast.makeText(getActivity(), "Invalid email or password", Toast.LENGTH_SHORT).show();
//                loadingDialog.dismiss();
            }
        });
    }

    private void setFragment(Fragment fragment, int fragmentNo){
        if(fragmentNo != currentFragment){
            currentFragment = fragmentNo;
            FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
            fragmentTransaction.replace(frameLayout.getId(), fragment);
            fragmentTransaction.commit();
        }
    }

    private BottomNavigationView.OnNavigationItemSelectedListener navListener =
            new BottomNavigationView.OnNavigationItemSelectedListener() {
                @Override
                public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                    Fragment selectedFragment = null;
                    switch (item.getItemId()) {
                        case R.id.nav_home:
                            selectedFragment = new HomeFragment();
                            break;
                        case R.id.nav_order_list:
                            selectedFragment = new OrderListFragment();
                            setTitle("Order List");
                            break;
                        case R.id.nav_create_order:
                            selectedFragment = new TodayOrdersFragment();
                            setTitle("Today's Order");
                            break;
                    }
                    getSupportFragmentManager().beginTransaction().replace(R.id.nav_host_fragment,
                            selectedFragment).commit();
                    return true;
                }
            };

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.home, menu);

        return true;
    }

    @Override
    public boolean onSupportNavigateUp() {
        NavController navController = Navigation.findNavController(this, R.id.nav_host_fragment);
        return NavigationUI.navigateUp(navController, mAppBarConfiguration)
                || super.onSupportNavigateUp();
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        int id = item.getItemId();
        switch (id){
            case R.id.action_logout:
                logout();
                finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    /*@Override
    protected void onDestroy() {
        super.onDestroy();
        logout();
    }*/

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        int id = item.getItemId();
        switch (id){
            case R.id.nav_home:
                invalidateOptionsMenu();
                setFragment(new HomeFragment(), HOME_FRAGMENT);
                break;
            case R.id.nav_today_order:
                invalidateOptionsMenu();
                gotoFragment("Today's Order", new TodayOrdersFragment(), ORDER_LIST_FRAGMENT);
                break;
            case R.id.nav_total_order:
                invalidateOptionsMenu();
                gotoFragment("Total Order", new OrderListFragment(), TOTAL_ORDER_FRAGMENT);
                break;
            case R.id.nav_delivery_report:
                invalidateOptionsMenu();
                gotoFragment("Delivery Report", new DeliveryFragment(), DELIVERY_REPORT);
                break;
            case R.id.nav_customer_report:
                invalidateOptionsMenu();
                gotoFragment("Customer Report", new CustomerFragment(), CUSTOMER_REPORT_FRAGMENT);
                break;
            case R.id.nav_pending_order:
                invalidateOptionsMenu();
                gotoFragment("Pending Order", new PendingOrderFragment(), PENDING_ORDER_FRAGMENT);
                break;

        }
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    private void gotoFragment(String title, Fragment fragment, int fragmentNo) {
        getSupportActionBar().setDisplayShowTitleEnabled(true);
        getSupportActionBar().setTitle(title);
        invalidateOptionsMenu();
        setFragment(fragment, fragmentNo);
    }

    private void logout() {
        /*String value = sharedPreferences.getString("ACCESS_KEY", "");
        return value;*/
        SharedPreferences sharedPreferences = getSharedPreferences(SHARED_PREFS, MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString("ACCESS_KEY", "");
        editor.apply();
        new com.farhad.sabtradecapital_deliveryman.auth.User(this).removeUser();
    }
    public String getUserName() {
        SharedPreferences sharedPreferences = getSharedPreferences(SHARED_PREFS, Context.MODE_PRIVATE);
        String value = sharedPreferences.getString("USERNAME", "");
        return value;
    }

    public void saveData(String accessKey, String user, String email) {
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString("ACCESS_KEY", accessKey);
        editor.putString("USERNAME", user);
        editor.apply();
//        Toast.makeText(this, "Login Successfull", Toast.LENGTH_SHORT).show();
    }

    public String getPassword() {
        String value = sharedPreferences.getString("PASSWORD", "");
        return value;
    }
    public String getUserEmail() {
        String value = sharedPreferences.getString("EMAIL", "");
        return value;
    }
}
