package com.farhad.sabtradecapital_deliveryman.auth;

import android.content.Context;
import android.content.SharedPreferences;

public class User {
    Context context;
    SharedPreferences sharedPreferences;

    private String name;
    private String date;

    public User(Context context) {
        this.context = context;
        sharedPreferences = context.getSharedPreferences("USER_INFO", Context.MODE_PRIVATE);
    }

    public String getName() {
        name = sharedPreferences.getString("NAME", "");
        return name;
    }

    public void setName(String name) {
        this.name = name;
        sharedPreferences.edit().putString("NAME", name).commit();
    }

    public String getDate() {
        date = sharedPreferences.getString("DATE", "");
        return date;
    }

    public void setDate(String date) {
        this.date = date;
        sharedPreferences.edit().putString("DATE", date).commit();
    }

    public void removeUser(){
        sharedPreferences.edit().clear().commit();
    }
}
