package com.farhad.sabtradecapital_deliveryman.auth;

public interface LogoutListener {
    void onSessionLogout();
}
