package com.farhad.sabtradecapital_deliveryman.models;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Customer {
    @SerializedName("user_id")
    @Expose
    private String userId;
    @SerializedName("area_id")
    @Expose
    private String areaId;
    @SerializedName("zone_id")
    @Expose
    private String zoneId;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("shop")
    @Expose
    private String shop;
    @SerializedName("phone")
    @Expose
    private String phone;
    @SerializedName("address")
    @Expose
    private String address;

    public Customer(String areaId, String zoneId, String name, String shop, String phone, String address) {
        this.areaId = areaId;
        this.zoneId = zoneId;
        this.name = name;
        this.shop = shop;
        this.phone = phone;
        this.address = address;
    }

    public Customer() {
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getAreaId() {
        return areaId;
    }

    public void setAreaId(String areaId) {
        this.areaId = areaId;
    }

    public String getZoneId() {
        return zoneId;
    }

    public void setZoneId(String zoneId) {
        this.zoneId = zoneId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getShop() {
        return shop;
    }

    public void setShop(String shop) {
        this.shop = shop;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    @Override
    public String toString() {
        return "Customer{" +
                "userId='" + userId + '\'' +
                ", areaId='" + areaId + '\'' +
                ", zoneId='" + zoneId + '\'' +
                ", name='" + name + '\'' +
                ", shop='" + shop + '\'' +
                ", phone='" + phone + '\'' +
                ", address='" + address + '\'' +
                '}';
    }
}