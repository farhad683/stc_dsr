package com.farhad.sabtradecapital_deliveryman.models;

public class DeliveryModel {
    private int id;
    private String date;
    private String shopName;
    private String customerName;
    private String sellBy;
    private double totalAmount;
    private double totalDue;
    private String address;
    private String status;

    public DeliveryModel(String date, String shopName, String customerName, String sellBy, double totalAmount, double totalDue, String address, String status) {
        this.date = date;
        this.shopName = shopName;
        this.customerName = customerName;
        this.sellBy = sellBy;
        this.totalAmount = totalAmount;
        this.totalDue = totalDue;
        this.address = address;
        this.status = status;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getShopName() {
        return shopName;
    }

    public void setShopName(String shopName) {
        this.shopName = shopName;
    }

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public String getSellBy() {
        return sellBy;
    }

    public void setSellBy(String sellBy) {
        this.sellBy = sellBy;
    }

    public double getTotalAmount() {
        return totalAmount;
    }

    public void setTotalAmount(double totalAmount) {
        this.totalAmount = totalAmount;
    }

    public double getTotalDue() {
        return totalDue;
    }

    public void setTotalDue(double totalDue) {
        this.totalDue = totalDue;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }
}