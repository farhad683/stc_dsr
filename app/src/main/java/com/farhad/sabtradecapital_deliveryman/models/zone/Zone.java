package com.farhad.sabtradecapital_deliveryman.models.zone;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Zone {

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("zone")
    @Expose
    private String zone;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getZone() {
        return zone;
    }

    public void setZone(String zone) {
        this.zone = zone;
    }

    @Override
    public String toString() {
        return "" + zone;
    }
}
