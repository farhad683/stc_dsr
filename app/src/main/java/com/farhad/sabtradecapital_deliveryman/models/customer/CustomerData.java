package com.farhad.sabtradecapital_deliveryman.models.customer;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class CustomerData {

    @SerializedName("data")
    @Expose
    private List<Customer> data = null;
    @SerializedName("http_status")
    @Expose
    private Integer httpStatus;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("access")
    @Expose
    private Boolean access;

    public List<Customer> getData() {
        return data;
    }

    public void setData(List<Customer> data) {
        this.data = data;
    }

    public Integer getHttpStatus() {
        return httpStatus;
    }

    public void setHttpStatus(Integer httpStatus) {
        this.httpStatus = httpStatus;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Boolean getAccess() {
        return access;
    }

    public void setAccess(Boolean access) {
        this.access = access;
    }

}