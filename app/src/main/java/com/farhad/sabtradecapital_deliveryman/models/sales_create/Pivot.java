package com.farhad.sabtradecapital_deliveryman.models.sales_create;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Pivot {

    @SerializedName("areable_id")
    @Expose
    private String areableId;
    @SerializedName("area_id")
    @Expose
    private String areaId;
    @SerializedName("areable_type")
    @Expose
    private String areableType;

    public String getAreableId() {
        return areableId;
    }

    public void setAreableId(String areableId) {
        this.areableId = areableId;
    }

    public String getAreaId() {
        return areaId;
    }

    public void setAreaId(String areaId) {
        this.areaId = areaId;
    }

    public String getAreableType() {
        return areableType;
    }

    public void setAreableType(String areableType) {
        this.areableType = areableType;
    }

}