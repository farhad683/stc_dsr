package com.farhad.sabtradecapital_deliveryman.models.delivery;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class DiscountModel {

    @SerializedName("discount")
    @Expose
    private String discount;

    public DiscountModel(String discount) {
        this.discount = discount;
    }

    public String getDiscount() {
        return discount;
    }

    public void setDiscount(String discount) {
        this.discount = discount;
    }

}