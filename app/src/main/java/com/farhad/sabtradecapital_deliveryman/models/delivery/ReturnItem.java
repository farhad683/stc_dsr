package com.farhad.sabtradecapital_deliveryman.models.delivery;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ReturnItem {

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("return")
    @Expose
    private Integer _return;

    public ReturnItem() {
    }

    public ReturnItem(Integer id, Integer _return) {
        this.id = id;
        this._return = _return;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getReturn() {
        return _return;
    }

    public void setReturn(Integer _return) {
        this._return = _return;
    }

}
