package com.farhad.sabtradecapital_deliveryman.models.category;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class CategoryData {

    @SerializedName("data")
    @Expose
    private List<Category> data = null;
    @SerializedName("http_status")
    @Expose
    private Integer httpStatus;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("access")
    @Expose
    private Boolean access;

    public List<Category> getData() {
        return data;
    }

    public void setData(List<Category> data) {
        this.data = data;
    }

    public Integer getHttpStatus() {
        return httpStatus;
    }

    public void setHttpStatus(Integer httpStatus) {
        this.httpStatus = httpStatus;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Boolean getAccess() {
        return access;
    }

    public void setAccess(Boolean access) {
        this.access = access;
    }

}
