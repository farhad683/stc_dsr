package com.farhad.sabtradecapital_deliveryman.models.sales_order;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class CreateSales {

    @SerializedName("area_id")
    @Expose
    private Integer areaId;
    @SerializedName("zone_id")
    @Expose
    private Integer zoneId;
    @SerializedName("customer_id")
    @Expose
    private Integer customerId;
    @SerializedName("cart")
    @Expose
    private List<Cart> cart = null;

    public Integer getAreaId() {
        return areaId;
    }

    public void setAreaId(Integer areaId) {
        this.areaId = areaId;
    }

    public Integer getZoneId() {
        return zoneId;
    }

    public void setZoneId(Integer zoneId) {
        this.zoneId = zoneId;
    }

    public Integer getCustomerId() {
        return customerId;
    }

    public void setCustomerId(Integer customerId) {
        this.customerId = customerId;
    }

    public List<Cart> getCart() {
        return cart;
    }

    public void setCart(List<Cart> cart) {
        this.cart = cart;
    }

}