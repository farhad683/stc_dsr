package com.farhad.sabtradecapital_deliveryman.models.invoice;
import com.farhad.sabtradecapital_deliveryman.models.sales.Cartoon;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Item {

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("product")
    @Expose
    private String product;
    @SerializedName("price")
    @Expose
    private String price;
    @SerializedName("tp")
    @Expose
    private String tp;
    @SerializedName("productCode")
    @Expose
    private String productCode;
    @SerializedName("cartoon")
    @Expose
    private Cartoon cartoon;
    @SerializedName("quantity")
    @Expose
    private String quantity;
    @SerializedName("total")
    @Expose
    private String total;
    @SerializedName("mrp")
    @Expose
    private String mrp;
    @SerializedName("dp")
    @Expose
    private String dp;
    @SerializedName("description")
    @Expose
    private Object description;
    @SerializedName("tax")
    @Expose
    private String tax;
    @SerializedName("sub_total")
    @Expose
    private String subTotal;

    @SerializedName("return")
    @Expose
    private Integer _return;


    public Integer get_return() {
        return _return;
    }

    public void set_return(Integer _return) {
        this._return = _return;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getProduct() {
        return product;
    }

    public void setProduct(String product) {
        this.product = product;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getTp() {
        return tp;
    }

    public void setTp(String tp) {
        this.tp = tp;
    }

    public String getProductCode() {
        return productCode;
    }

    public void setProductCode(String productCode) {
        this.productCode = productCode;
    }

    public Cartoon getCartoon() {
        return cartoon;
    }

    public void setCartoon(Cartoon cartoon) {
        this.cartoon = cartoon;
    }

    public String getQuantity() {
        return quantity;
    }

    public void setQuantity(String quantity) {
        this.quantity = quantity;
    }

    public String getTotal() {
        return total;
    }

    public void setTotal(String total) {
        this.total = total;
    }

    public String getMrp() {
        return mrp;
    }

    public void setMrp(String mrp) {
        this.mrp = mrp;
    }

    public String getDp() {
        return dp;
    }

    public void setDp(String dp) {
        this.dp = dp;
    }

    public Object getDescription() {
        return description;
    }

    public void setDescription(Object description) {
        this.description = description;
    }

    public String getTax() {
        return tax;
    }

    public void setTax(String tax) {
        this.tax = tax;
    }

    public String getSubTotal() {
        return subTotal;
    }

    public void setSubTotal(String subTotal) {
        this.subTotal = subTotal;
    }
}