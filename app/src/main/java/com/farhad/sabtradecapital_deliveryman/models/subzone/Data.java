package com.farhad.sabtradecapital_deliveryman.models.subzone;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Data {

    @SerializedName("sub_zone")
    @Expose
    private List<SubZone> subZone = null;

    public List<SubZone> getSubZone() {
        return subZone;
    }

    public void setSubZone(List<SubZone> subZone) {
        this.subZone = subZone;
    }

}