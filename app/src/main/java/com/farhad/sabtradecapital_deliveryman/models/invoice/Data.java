package com.farhad.sabtradecapital_deliveryman.models.invoice;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Data {

    @SerializedName("data")
    @Expose
    private Invoice data;
    @SerializedName("http_status")
    @Expose
    private Integer httpStatus;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("access")
    @Expose
    private Boolean access;

    public Invoice getData() {
        return data;
    }

    public void setData(Invoice data) {
        this.data = data;
    }

    public Integer getHttpStatus() {
        return httpStatus;
    }

    public void setHttpStatus(Integer httpStatus) {
        this.httpStatus = httpStatus;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Boolean getAccess() {
        return access;
    }

    public void setAccess(Boolean access) {
        this.access = access;
    }

}
