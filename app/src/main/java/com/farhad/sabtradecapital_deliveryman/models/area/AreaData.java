package com.farhad.sabtradecapital_deliveryman.models.area;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class AreaData {

    @SerializedName("data")
    @Expose
    private List<Area> data = null;
    @SerializedName("http_status")
    @Expose
    private String httpStatus;

    public List<Area> getData() {
        return data;
    }

    public void setData(List<Area> data) {
        this.data = data;
    }

    public String getHttpStatus() {
        return httpStatus;
    }

    public void setHttpStatus(String httpStatus) {
        this.httpStatus = httpStatus;
    }

}