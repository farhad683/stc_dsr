package com.farhad.sabtradecapital_deliveryman.models.customer;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Item {

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("quantity")
    @Expose
    private String quantity;
    @SerializedName("tp")
    @Expose
    private String tp;
    @SerializedName("mrp")
    @Expose
    private String mrp;
    @SerializedName("dp")
    @Expose
    private String dp;
    @SerializedName("product")
    @Expose
    private String product;
    @SerializedName("description")
    @Expose
    private Object description;
    @SerializedName("tax")
    @Expose
    private Integer tax;
    @SerializedName("total")
    @Expose
    private Integer total;
    @SerializedName("sub_total")
    @Expose
    private Integer subTotal;
    @SerializedName("price")
    @Expose
    private String price;
    @SerializedName("productCode")
    @Expose
    private String productCode;
    @SerializedName("cartoon")
    @Expose
    private Cartoon cartoon;
    @SerializedName("return")
    @Expose
    private String _return;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getQuantity() {
        return quantity;
    }

    public void setQuantity(String quantity) {
        this.quantity = quantity;
    }

    public String getTp() {
        return tp;
    }

    public void setTp(String tp) {
        this.tp = tp;
    }

    public String getMrp() {
        return mrp;
    }

    public void setMrp(String mrp) {
        this.mrp = mrp;
    }

    public String getDp() {
        return dp;
    }

    public void setDp(String dp) {
        this.dp = dp;
    }

    public String getProduct() {
        return product;
    }

    public void setProduct(String product) {
        this.product = product;
    }

    public Object getDescription() {
        return description;
    }

    public void setDescription(Object description) {
        this.description = description;
    }

    public Integer getTax() {
        return tax;
    }

    public void setTax(Integer tax) {
        this.tax = tax;
    }

    public Integer getTotal() {
        return total;
    }

    public void setTotal(Integer total) {
        this.total = total;
    }

    public Integer getSubTotal() {
        return subTotal;
    }

    public void setSubTotal(Integer subTotal) {
        this.subTotal = subTotal;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getProductCode() {
        return productCode;
    }

    public void setProductCode(String productCode) {
        this.productCode = productCode;
    }

    public Cartoon getCartoon() {
        return cartoon;
    }

    public void setCartoon(Cartoon cartoon) {
        this.cartoon = cartoon;
    }

    public String getReturn() {
        return _return;
    }

    public void setReturn(String _return) {
        this._return = _return;
    }

}