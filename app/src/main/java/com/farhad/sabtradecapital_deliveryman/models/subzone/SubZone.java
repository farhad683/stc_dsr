package com.farhad.sabtradecapital_deliveryman.models.subzone;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class SubZone {

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("sub_zone")
    @Expose
    private String subZone;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getSubZone() {
        return subZone;
    }

    public void setSubZone(String subZone) {
        this.subZone = subZone;
    }

    @Override
    public String toString() {
        return subZone + "";
    }
}