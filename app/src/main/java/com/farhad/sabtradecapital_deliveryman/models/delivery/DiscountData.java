package com.farhad.sabtradecapital_deliveryman.models.delivery;
import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class DiscountData {

    @SerializedName("data")
    @Expose
    private List<Object> data = null;
    @SerializedName("http_status")
    @Expose
    private Integer httpStatus;
    @SerializedName("message")
    @Expose
    private String message;

    public List<Object> getData() {
        return data;
    }

    public void setData(List<Object> data) {
        this.data = data;
    }

    public Integer getHttpStatus() {
        return httpStatus;
    }

    public void setHttpStatus(Integer httpStatus) {
        this.httpStatus = httpStatus;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

}