package com.farhad.sabtradecapital_deliveryman.models.sales_create;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Data {

    @SerializedName("areas")
    @Expose
    private List<Area> areas = null;
    @SerializedName("categories")
    @Expose
    private List<Category> categories = null;

    public List<Area> getAreas() {
        return areas;
    }

    public void setAreas(List<Area> areas) {
        this.areas = areas;
    }

    public List<Category> getCategories() {
        return categories;
    }

    public void setCategories(List<Category> categories) {
        this.categories = categories;
    }

}