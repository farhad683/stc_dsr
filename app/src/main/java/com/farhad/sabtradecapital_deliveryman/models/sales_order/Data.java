package com.farhad.sabtradecapital_deliveryman.models.sales_order;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Data {

    @SerializedName("customer_id")
    @Expose
    private Integer customerId;
    @SerializedName("items")
    @Expose
    private List<Item> items = null;
    @SerializedName("total_tp")
    @Expose
    private String totalTp;
    @SerializedName("total_mrp")
    @Expose
    private String totalMrp;
    @SerializedName("total_dp")
    @Expose
    private String totalDp;
    @SerializedName("zone_id")
    @Expose
    private String zoneId;
    @SerializedName("user_id")
    @Expose
    private Integer userId;
    @SerializedName("updated_at")
    @Expose
    private String updatedAt;
    @SerializedName("created_at")
    @Expose
    private String createdAt;
    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("invoice")
    @Expose
    private String invoice;

    public Integer getCustomerId() {
        return customerId;
    }

    public void setCustomerId(Integer customerId) {
        this.customerId = customerId;
    }

    public List<Item> getItems() {
        return items;
    }

    public void setItems(List<Item> items) {
        this.items = items;
    }

    public String getTotalTp() {
        return totalTp;
    }

    public void setTotalTp(String totalTp) {
        this.totalTp = totalTp;
    }

    public String getTotalMrp() {
        return totalMrp;
    }

    public void setTotalMrp(String totalMrp) {
        this.totalMrp = totalMrp;
    }

    public String getTotalDp() {
        return totalDp;
    }

    public void setTotalDp(String totalDp) {
        this.totalDp = totalDp;
    }

    public String getZoneId() {
        return zoneId;
    }

    public void setZoneId(String zoneId) {
        this.zoneId = zoneId;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getInvoice() {
        return invoice;
    }

    public void setInvoice(String invoice) {
        this.invoice = invoice;
    }

    @Override
    public String toString() {
        return "Data{" +
                "customerId=" + customerId +
                ", items=" + items +
                ", totalTp=" + totalTp +
                ", totalMrp=" + totalMrp +
                ", totalDp=" + totalDp +
                ", zoneId='" + zoneId + '\'' +
                ", userId=" + userId +
                ", updatedAt='" + updatedAt + '\'' +
                ", createdAt='" + createdAt + '\'' +
                ", id=" + id +
                ", invoice='" + invoice + '\'' +
                '}';
    }
}