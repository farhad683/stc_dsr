package com.farhad.sabtradecapital_deliveryman.models.delivery;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class DeliveryData {

    @SerializedName("payment")
    @Expose
    private String payment;
    @SerializedName("returnItems")
    @Expose
    private List<ReturnItem> returnItems = null;

    public String getPayment() {
        return payment;
    }

    public void setPayment(String payment) {
        this.payment = payment;
    }

    public List<ReturnItem> getReturnItems() {
        return returnItems;
    }

    public void setReturnItems(List<ReturnItem> returnItems) {
        this.returnItems = returnItems;
    }

}
