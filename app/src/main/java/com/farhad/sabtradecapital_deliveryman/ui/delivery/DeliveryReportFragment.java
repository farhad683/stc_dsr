package com.farhad.sabtradecapital_deliveryman.ui.delivery;

import android.app.Dialog;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.farhad.sabtradecapital_deliveryman.R;
import com.farhad.sabtradecapital_deliveryman.adapter.DeliveredInvoiceAdapter;
import com.farhad.sabtradecapital_deliveryman.adapter.InvoiceAdapter;
import com.farhad.sabtradecapital_deliveryman.models.invoice.Invoice;
import com.farhad.sabtradecapital_deliveryman.models.invoice.Item;
import com.farhad.sabtradecapital_deliveryman.ui.order.ConfirmOrderFragment;
import com.farhad.sabtradecapital_deliveryman.ui.order.OrderListFragment;
import com.farhad.sabtradecapital_deliveryman.viewmodels.CartViewModel;
import com.farhad.sabtradecapital_deliveryman.viewmodels.InvoiceViewModel;

/**
 * A simple {@link Fragment} subclass.
 */
public class DeliveryReportFragment extends Fragment {

    private static final String SALES_ID = "sales_id";
    private int id;
    private Dialog loadingDialog;
    private Button custDailyGoBackBtn;

    private double totalAmount = 0;

    // adapters
    private DeliveredInvoiceAdapter invoiceAdapter;

    public static DeliveryReportFragment newInstance(int id) {
        DeliveryReportFragment fragment = new DeliveryReportFragment();
        Bundle args = new Bundle();
        args.putInt(SALES_ID, id);
        fragment.setArguments(args);
        return fragment;
    }

    public DeliveryReportFragment() {
        // Required empty public constructor
    }

    // viewmodels
    private InvoiceViewModel invoiceViewModel;
    private CartViewModel cartViewModel;

    private TextView cust_det_tv_name, cust_det_tv_address, cust_det_tv_phone, cust_det_tv_email, cust_det_date;
    private TextView cust_det_invoice_id, custDailyTotalText;
    private TextView cust_due;
    private RecyclerView recyclerViewProductItems;

    private int returnQuantity;
    private int paidAmount = 0;

    private Button confirmBtn;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_delivery_report, container, false);
        invoiceViewModel = new ViewModelProvider(getActivity()).get(InvoiceViewModel.class);
        cartViewModel = new ViewModelProvider(getActivity()).get(CartViewModel.class);

        if (getArguments() != null) {
            id = getArguments().getInt(SALES_ID);
        }

        initializeViews(view);
        loadingDialog.show();
        setUpPage();
        return view;
    }

    private void setUpPage() {
        invoiceViewModel.getSales(id).observe(getActivity(), new Observer<Invoice>() {
            @Override
            public void onChanged(Invoice invoice) {

                loadingDialog.dismiss();

                cust_det_tv_name.setText("" + invoice.getCustomer().getShop());
                cust_det_tv_address.setText("Address: " + invoice.getCustomer().getAddress());
                cust_det_tv_phone.setText("" + invoice.getCustomer().getPhone());
                cust_det_tv_email.setText("");
                cust_det_date.setText("" + invoice.getCreatedAt());


                invoiceAdapter = new DeliveredInvoiceAdapter(invoice.getItems());
                recyclerViewProductItems.setHasFixedSize(true);
                RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getContext());
                recyclerViewProductItems.setLayoutManager(layoutManager);
                recyclerViewProductItems.setAdapter(invoiceAdapter);
                invoiceAdapter.notifyDataSetChanged();

                totalAmount = Double.parseDouble(invoice.getTotalTp());
                custDailyTotalText.setText("Total: " + totalAmount + " \u09F3");
                totalAmount = Double.parseDouble(invoice.getTotalTp());

            }
        });
    }

    private void showInputDialog(Item item, int position) {
        final Dialog signInDialog = new Dialog(getContext());
        signInDialog.setContentView(R.layout.add_quantity_dialog_layout);
        signInDialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        signInDialog.setCancelable(true);
        final Button dialogOK = signInDialog.findViewById(R.id.add_quantity_ok_btn);
        final EditText edtQuantity = signInDialog.findViewById(R.id.add_quantity_input_edt);

        dialogOK.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                returnQuantity = Integer.parseInt(edtQuantity.getText().toString());
                if (returnQuantity <= Integer.parseInt(item.getQuantity())) {
//                    cartList.add(new Cart(pid, quantity));
                    com.farhad.sabtradecapital_deliveryman.models.Cart car = new com.farhad.sabtradecapital_deliveryman.models.Cart(item.getId(), item.getProduct(), returnQuantity);
                    cartViewModel.insertCart(car);

                    double returnedPrice = ((Double.parseDouble(item.getTp()))*returnQuantity);
                    Double total = Double.parseDouble(item.getTotal()) - returnedPrice;
                    item.setTotal("" + total.floatValue());

                    int qty = Integer.parseInt(item.getQuantity());
                    item.setQuantity("" + (qty -returnQuantity));

                    totalAmount = totalAmount - returnedPrice;
                    custDailyTotalText.setText("" + totalAmount + " \u09F3");
                    invoiceAdapter.notifyDataSetChanged();
                    signInDialog.dismiss();

                    Toast.makeText(getContext(), "" + returnQuantity + " product will be returned", Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(getContext(), "Only " + Integer.parseInt(item.getQuantity()) + " product is ordered", Toast.LENGTH_SHORT).show();
                }
            }
        });


        signInDialog.show();
    }
    private void showInputDialog() {
        final Dialog signInDialog = new Dialog(getContext());
        signInDialog.setContentView(R.layout.add_quantity_dialog_layout);
        signInDialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        signInDialog.setCancelable(true);
        final Button dialogOK = signInDialog.findViewById(R.id.add_quantity_ok_btn);
        final EditText edtQuantity = signInDialog.findViewById(R.id.add_quantity_input_edt);

        dialogOK.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                paidAmount = Integer.parseInt(edtQuantity.getText().toString());
                Toast.makeText(getContext(), "" + paidAmount + " paid", Toast.LENGTH_SHORT).show();
                signInDialog.dismiss();
                /*if (returnQuantity <= Integer.parseInt(item.getQuantity())) {
//                    cartList.add(new Cart(pid, quantity));
                    com.farhad.sabtradecapital_deliveryman.models.Cart car = new com.farhad.sabtradecapital_deliveryman.models.Cart(item.getId(), item.getProduct(), returnQuantity);
                    cartViewModel.insertCart(car);
                    signInDialog.dismiss();
                    Toast.makeText(getContext(), "" + returnQuantity + " product will be returned", Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(getContext(), "Only " + Integer.parseInt(item.getQuantity()) + " product is ordered", Toast.LENGTH_SHORT).show();
                }*/
            }
        });


        signInDialog.show();
    }

    private void initializeViews(View view) {
        cust_det_tv_name = view.findViewById(R.id.del_det_tv_name);
        cust_det_tv_address = view.findViewById(R.id.del_det_tv_address);
        cust_det_tv_phone = view.findViewById(R.id.del_det_tv_phone);
        cust_det_tv_email = view.findViewById(R.id.del_det_tv_email);
        cust_det_date = view.findViewById(R.id.del_det_date);
        cust_det_invoice_id = view.findViewById(R.id.del_det_invoice_id);
        recyclerViewProductItems = view.findViewById(R.id.del_det_recycler_cust);
        custDailyTotalText = view.findViewById(R.id.del_daily_total);
        custDailyGoBackBtn = view.findViewById(R.id.del_daily_go_back_btn);

        custDailyGoBackBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                cartViewModel.deleteAllCart();
                goToFragment(new DeliveryFragment());
            }
        });

        initializeDialog();
    }
    private void goToFragment(Fragment newFragment) {
        FragmentManager fm = getActivity().getSupportFragmentManager();
        FragmentTransaction transaction = fm.beginTransaction();
        transaction.replace(R.id.nav_host_fragment, newFragment);
        transaction.addToBackStack(null);
        transaction.commit();
//        fm.popBackStack();
    }

    private void initializeDialog() {
        loadingDialog = new Dialog(getContext());
        loadingDialog.setContentView(R.layout.loading_dialog);
        loadingDialog.getWindow().setLayout(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        loadingDialog.setCancelable(true);
    }
}