package com.farhad.sabtradecapital_deliveryman.ui.order;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.Dialog;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.farhad.sabtradecapital_deliveryman.R;
import com.farhad.sabtradecapital_deliveryman.adapter.TodayOrderAdapter;
import com.farhad.sabtradecapital_deliveryman.models.sales.Sales;
import com.farhad.sabtradecapital_deliveryman.viewmodels.SalesViewModel;

import java.util.ArrayList;
import java.util.List;

public class PendingOrderActivity extends AppCompatActivity {

    private Dialog loadingDialog;

    private RecyclerView rvOrderList;
    private TextView order_list_total_text;
    private ArrayList<OrderModel> orderLists;
    private TodayOrderAdapter salesListAdapter;

    private SalesViewModel salesViewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order_list);
        setTitle("Pending Order");

        salesViewModel = new ViewModelProvider(this).get(SalesViewModel.class);

        initializeOrders();
    }
    private void initializeOrders() {
        initializeDialog();

        rvOrderList = findViewById(R.id.activity_order_list_rv_details);
        order_list_total_text = findViewById(R.id.order_list_total_text);
        orderLists = new ArrayList<>();

        salesViewModel.getAllSales().observe(this, new Observer<List<Sales>>() {
            @Override
            public void onChanged(List<Sales> salesList) {
                List<Sales> sale = new ArrayList<>();
                loadingDialog.dismiss();
                for(Sales sales: salesList){
                    if(!sales.getDeliveryStatus().equals("delivered") && sales.getCustomer() != null){
                        sale.add(sales);
                    }
                }
                salesListAdapter = new TodayOrderAdapter(PendingOrderActivity.this, sale);

//                rvOrderList.setHasFixedSize(true);
                rvOrderList.setLayoutManager(new LinearLayoutManager(PendingOrderActivity.this));
                rvOrderList.setAdapter(salesListAdapter);
                salesListAdapter.notifyDataSetChanged();

                double total = 0.0;
                for(Sales sales: salesList){
                    if(sales.getTotalTp() != null){
                        total += Double.parseDouble(sales.getTotalTp());
                    }
                }

                order_list_total_text.setText("Total: " + (float)total  + " \u09F3");
            }
        });
    }

    private void initializeDialog() {
        loadingDialog = new Dialog(this);
        loadingDialog.setContentView(R.layout.loading_dialog);
        loadingDialog.getWindow().setLayout(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        loadingDialog.setCancelable(true);

        loadingDialog.show();
    }
}