package com.farhad.sabtradecapital_deliveryman.ui.delivery;

import android.app.Dialog;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.farhad.sabtradecapital_deliveryman.R;
import com.farhad.sabtradecapital_deliveryman.adapter.TodayOrderAdapter;
import com.farhad.sabtradecapital_deliveryman.models.sales.Sales;
import com.farhad.sabtradecapital_deliveryman.ui.order.OrderModel;
import com.farhad.sabtradecapital_deliveryman.viewmodels.SalesViewModel;

import java.util.ArrayList;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link PendingOrderFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class PendingOrderFragment extends Fragment {

    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    public PendingOrderFragment() {
        // Required empty public constructor
    }

    public static PendingOrderFragment newInstance(String param1, String param2) {
        PendingOrderFragment fragment = new PendingOrderFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    private Dialog loadingDialog;

    private RecyclerView rvOrderList;
    private TextView order_list_total_text;
    private ArrayList<OrderModel> orderLists;
    private TodayOrderAdapter salesListAdapter;

    private SalesViewModel salesViewModel;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        salesViewModel = new ViewModelProvider(this).get(SalesViewModel.class);
        View view = inflater.inflate(R.layout.activity_order_list, container, false);


        initializeOrders(view);
        return view;
    }

    private void initializeOrders(View view) {
        initializeDialog();

        rvOrderList = view.findViewById(R.id.activity_order_list_rv_details);
        order_list_total_text = view.findViewById(R.id.order_list_total_text);
        orderLists = new ArrayList<>();

        salesViewModel.getAllSales().observe(getViewLifecycleOwner(), new Observer<List<Sales>>() {
            @Override
            public void onChanged(List<Sales> salesList) {
                List<Sales> sale = new ArrayList<>();
                loadingDialog.dismiss();
                double total = 0.0;
                for(Sales sales: salesList){
                    if(!sales.getDeliveryStatus().equals("delivered") && sales.getCustomer() != null){
                        sale.add(sales);
                    }
                    if(sales.getTotalTp() != null){
                        total += Double.parseDouble(sales.getTotalTp());
                    }
                }
                salesListAdapter = new TodayOrderAdapter(getContext(), sale);

//                rvOrderList.setHasFixedSize(true);
                rvOrderList.setLayoutManager(new LinearLayoutManager(getActivity()));
                rvOrderList.setAdapter(salesListAdapter);
                salesListAdapter.notifyDataSetChanged();


                /*for(Sales sales: salesList){
                    if(sales.getTotalTp() != null){
                        total += Double.parseDouble(sales.getTotalTp());
                    }
                }*/

                order_list_total_text.setText("Total: " + (float)total  + " \u09F3");
            }
        });
    }

    private void initializeDialog() {
        loadingDialog = new Dialog(getContext());
        loadingDialog.setContentView(R.layout.loading_dialog);
        loadingDialog.getWindow().setLayout(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        loadingDialog.setCancelable(true);

        loadingDialog.show();
    }
}