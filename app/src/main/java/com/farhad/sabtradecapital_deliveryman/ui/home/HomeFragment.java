package com.farhad.sabtradecapital_deliveryman.ui.home;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.lifecycle.ViewModelProviders;

import com.farhad.sabtradecapital_deliveryman.R;
import com.farhad.sabtradecapital_deliveryman.api.ApiRef;
import com.farhad.sabtradecapital_deliveryman.api.ApiServices;
import com.farhad.sabtradecapital_deliveryman.models.User;
import com.farhad.sabtradecapital_deliveryman.ui.delivery.DeliveryFragment;
import com.farhad.sabtradecapital_deliveryman.ui.customer.CustomerFragment;
import com.farhad.sabtradecapital_deliveryman.ui.delivery.PendingOrderFragment;
import com.farhad.sabtradecapital_deliveryman.ui.order.OrderListFragment;
import com.farhad.sabtradecapital_deliveryman.ui.order.TodayOrdersFragment;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.content.Context.MODE_PRIVATE;
import static com.farhad.sabtradecapital_deliveryman.LoginActivity.SHARED_PREFS;

public class HomeFragment extends Fragment implements View.OnClickListener {
    private LinearLayout lt_today_order, lt_total_order, lt_delivery_report, lt_sales_report, lt_customer_report, lt_customer_details;

    private HomeViewModel homeViewModel;
    private ApiServices apiServices;


    private SharedPreferences sharedPreferences;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        homeViewModel =
                ViewModelProviders.of(this).get(HomeViewModel.class);
        final View root = inflater.inflate(R.layout.fragment_home, container, false);
        sharedPreferences = this.getActivity().getSharedPreferences(SHARED_PREFS, Context.MODE_PRIVATE);
        apiServices = ApiRef.getInstance().create(ApiServices.class);
        //login();
        getActivity().setTitle("DSR - " + getUserName());

        lt_today_order = root.findViewById(R.id.lt_today_order);
        lt_total_order = root.findViewById(R.id.lt_total_order);
        lt_delivery_report = root.findViewById(R.id.lt_delivery_report);
        lt_sales_report = root.findViewById(R.id.lt_sales_report);
        lt_customer_report = root.findViewById(R.id.lt_customer_report);
        lt_customer_details = root.findViewById(R.id.lt_customer_details);


        lt_today_order.setOnClickListener(this);
        lt_total_order.setOnClickListener(this);
        lt_delivery_report.setOnClickListener(this);
        lt_sales_report.setOnClickListener(this);
        lt_customer_report.setOnClickListener(this);
        lt_customer_details.setOnClickListener(this);
        /*final TextView textView = root.findViewById(R.id.text_home);
        homeViewModel.getText().observe(getViewLifecycleOwner(), new Observer<String>() {
            @Override
            public void onChanged(@Nullable String s) {
                textView.setText(s);
            }
        });*/
        return root;
    }


    private void login() {
        apiServices.login(getUserEmail(), getPassword()).enqueue(new Callback<User>() {
            @Override
            public void onResponse(Call<User> call, Response<User> response) {
                if(response.isSuccessful()){
                    if(response.code() == 200){
                        saveData(response.body().getData().getAccessToken(), response.body().getData().getUser().getName(), response.body().getData().getUser().getEmail());
//                        startActivity(new Intent(LoginActivity.this, HomeActivity.class));
//                        loadingDialog.dismiss();
//                        finish();
                    }
                    Log.d("TAG", "onResponse: " + response.body().getData().getAccessToken());
                } else {
                    Toast.makeText(getActivity(), "Invalid email or password", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<User> call, Throwable t) {
                Log.d("TAG", "onResponse: " + t.getMessage());
                Log.d("TAG", "onResponse: " + t.getMessage());
//                Toast.makeText(getActivity(), "Invalid email or password", Toast.LENGTH_SHORT).show();
//                loadingDialog.dismiss();
            }
        });
    }
    public void saveData(String accessKey, String user, String email) {
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString("ACCESS_KEY", accessKey);
        editor.putString("USERNAME", user);
        editor.putString("EMAIL", email);
        editor.apply();
//        Toast.makeText(this, "Login Successfull", Toast.LENGTH_SHORT).show();
    }

    public String getPassword() {
        String value = sharedPreferences.getString("PASSWORD", "");
        return value;
    }
    public String getUserEmail() {
        String value = sharedPreferences.getString("EMAIL", "");
        return value;
    }

    @Override
    public void onResume() {
        super.onResume();
        getActivity().setTitle("DSR - " + getUserName());
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();
        FragmentManager fm = getActivity().getSupportFragmentManager();
        switch (id){
            case R.id.lt_today_order:
                fm.beginTransaction().addToBackStack(null).replace(R.id.nav_host_fragment, new TodayOrdersFragment()).commit();
                getActivity().setTitle("Today's Order");
                break;
            case R.id.lt_total_order:
                fm.beginTransaction().addToBackStack(null).replace(R.id.nav_host_fragment, new OrderListFragment()).commit();
                getActivity().setTitle("All Order");
                break;
            case R.id.lt_delivery_report:
                fm.beginTransaction().addToBackStack(null).replace(R.id.nav_host_fragment, new DeliveryFragment()).commit();
                getActivity().setTitle("Delivery Report");
                break;
            case R.id.lt_sales_report:
//                fm.beginTransaction().addToBackStack(null).replace(R.id.nav_host_fragment, new SalesOrderFragment()).commit();
                break;
            case R.id.lt_customer_report:
                fm.beginTransaction().addToBackStack(null).replace(R.id.nav_host_fragment, new PendingOrderFragment()).commit();
                break;
            case R.id.lt_customer_details:
                fm.beginTransaction().addToBackStack(null).replace(R.id.nav_host_fragment, new CustomerFragment()).commit();
                break;

        }
    }
    public String getUserName() {
        SharedPreferences sharedPreferences = getActivity().getSharedPreferences(SHARED_PREFS, MODE_PRIVATE);
        String value = sharedPreferences.getString("USERNAME", "");
        return value;
    }
}
