package com.farhad.sabtradecapital_deliveryman.ui.customer;

import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.farhad.sabtradecapital_deliveryman.R;
import com.farhad.sabtradecapital_deliveryman.adapter.CustomerSalesAdapter;
import com.farhad.sabtradecapital_deliveryman.models.customer.Data;
import com.farhad.sabtradecapital_deliveryman.models.customer.Sale;
import com.farhad.sabtradecapital_deliveryman.ui.TicketFragment;
import com.farhad.sabtradecapital_deliveryman.viewmodels.CustomerViewModel;

import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 */
public class CustomerDetailsFragment extends Fragment {
    private static final String CUST_ID = "CUST_ID";
    private int CUSTOMER_ID;

    public CustomerDetailsFragment() {
        // Required empty public constructor
    }

    public static CustomerDetailsFragment newInstance(int id) {
        CustomerDetailsFragment fragment = new CustomerDetailsFragment();
        Bundle args = new Bundle();
        args.putInt(CUST_ID, id);
        fragment.setArguments(args);
        return fragment;
    }

    private TextView custNameTV, custAddress, cust_det_tv_phone, cust_det_tv_email, cust_det_tv_total_sales;
    private TextView cust_det_tv_due;
    private Button cust_det_end_date, cust_det_start_date_btn;
    private RecyclerView cust_det_recycler;

    // viewmodels
    private CustomerViewModel customerViewModel;

    // adapter
    private CustomerSalesAdapter adapter;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_customer_details, container, false);
        getActivity().setTitle("Customer Details");

        customerViewModel = new ViewModelProvider(getActivity()).get(CustomerViewModel.class);

        if(getArguments() != null){
            CUSTOMER_ID = getArguments().getInt(CUST_ID, 0);
        }

        inititializeViews(view);
        if(CUSTOMER_ID != 0){
            setupPage(view);
        }

        Log.d("TAG", "onCreateView: " + CUSTOMER_ID);
//        Toast.makeText(getContext(), "" + CUSTOMER_ID, Toast.LENGTH_SHORT).show();

        return view;
    }

    private void setupPage(View view) {
        customerViewModel.getCustomer(CUSTOMER_ID).observe(getViewLifecycleOwner(), new Observer<Data>() {
            @Override
            public void onChanged(Data data) {
                custNameTV.setText("" + data.getShop());
                custAddress.setText(data.getAddress());
                cust_det_tv_phone.setText(data.getPhone());
                cust_det_tv_email.setText(""); //===================
                if(!data.getSales().isEmpty()){
                    double total = 0.0;
                    for(Sale sale: data.getSales()){
                        total+= Double.parseDouble(sale.getTotalTp());
                    }
                    cust_det_tv_total_sales.setText("Total: " + total + " \u09F3");
                } else {
                    cust_det_tv_total_sales.setText("Total: 0.0" + " \u09F3");
                }
                cust_det_tv_due.setText("Due: " + data.getDue()  + " \u09F3");
                cust_det_start_date_btn.setText("" + data.getCreatedAt());
                cust_det_end_date.setText("" + data.getUpdatedAt());



                setSalesData(data.getSales());

            }
        });
    }

    private void setSalesData(List<Sale> sales) {
        adapter = new CustomerSalesAdapter(getContext(), sales);
        cust_det_recycler.setHasFixedSize(true);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getContext());
        cust_det_recycler.setLayoutManager(layoutManager);
        cust_det_recycler.setAdapter(adapter);
        adapter.notifyDataSetChanged();
    }

    private void inititializeViews(View view) {
        custNameTV = view.findViewById(R.id.cust_det_tv_name);
        custAddress = view.findViewById(R.id.cust_det_tv_address);
        cust_det_tv_phone = view.findViewById(R.id.cust_det_tv_phone);
        cust_det_tv_email = view.findViewById(R.id.cust_det_tv_email);
        cust_det_tv_total_sales = view.findViewById(R.id.cust_det_tv_total_sales);
        cust_det_tv_due = view.findViewById(R.id.cust_det_tv_due);
        cust_det_end_date = view.findViewById(R.id.cust_det_end_date_btn);
        cust_det_start_date_btn = view.findViewById(R.id.cust_det_start_date_btn);
        cust_det_recycler = view.findViewById(R.id.cust_det_recycler);
    }


}
