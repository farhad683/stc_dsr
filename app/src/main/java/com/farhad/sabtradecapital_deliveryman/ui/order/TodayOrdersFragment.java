package com.farhad.sabtradecapital_deliveryman.ui.order;

import android.app.Dialog;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.farhad.sabtradecapital_deliveryman.R;
import com.farhad.sabtradecapital_deliveryman.adapter.OrderListAdapter;
import com.farhad.sabtradecapital_deliveryman.adapter.TodayOrderAdapter;
import com.farhad.sabtradecapital_deliveryman.models.sales.Sales;
import com.farhad.sabtradecapital_deliveryman.viewmodels.SalesViewModel;

import java.util.ArrayList;
import java.util.List;

public class TodayOrdersFragment extends Fragment {
    private Dialog loadingDialog;

    private RecyclerView rvOrderList;
    private TextView order_list_total_text;
    private ArrayList<OrderModel> orderLists;
    private TodayOrderAdapter salesListAdapter;

    private SalesViewModel salesViewModel;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        salesViewModel = new ViewModelProvider(this).get(SalesViewModel.class);

        View root = inflater.inflate(R.layout.activity_order_list, container, false);

        initializeOrders(root);
        return root;
    }

    private void initializeOrders(View view) {
        initializeDialog();

        rvOrderList = view.findViewById(R.id.activity_order_list_rv_details);
        order_list_total_text = view.findViewById(R.id.order_list_total_text);
        orderLists = new ArrayList<>();

        salesViewModel.getTodaysSales().observe(getViewLifecycleOwner(), new Observer<List<Sales>>() {
            @Override
            public void onChanged(List<Sales> salesList) {
                List<Sales> sale = new ArrayList<>();
                loadingDialog.dismiss();
                double total = 0.0;
                for(Sales sales: salesList){
                    if(!sales.getDeliveryStatus().equals("delivered") && sales.getCustomer() != null){
                        sale.add(sales);
                        if(sales.getTotalTp() != null){
                            total += Double.parseDouble(sales.getTotalTp());
                        }
                    }


                }
                salesListAdapter = new TodayOrderAdapter(getContext(), sale);

//                rvOrderList.setHasFixedSize(true);
                rvOrderList.setLayoutManager(new LinearLayoutManager(getActivity()));
                rvOrderList.setAdapter(salesListAdapter);
                salesListAdapter.notifyDataSetChanged();




                order_list_total_text.setText("Total: " + total  + " \u09F3");
            }
        });
    }

    private void initializeDialog() {
        loadingDialog = new Dialog(getContext());
        loadingDialog.setContentView(R.layout.loading_dialog);
        loadingDialog.getWindow().setLayout(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        loadingDialog.setCancelable(true);

        loadingDialog.show();
    }
}
