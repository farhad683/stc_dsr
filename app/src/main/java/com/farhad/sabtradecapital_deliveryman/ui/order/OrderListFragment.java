package com.farhad.sabtradecapital_deliveryman.ui.order;

import android.app.Dialog;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.farhad.sabtradecapital_deliveryman.R;
import com.farhad.sabtradecapital_deliveryman.adapter.OrderListAdapter;
import com.farhad.sabtradecapital_deliveryman.models.sales.Sales;
import com.farhad.sabtradecapital_deliveryman.ui.customer.CustomerDailyDeitalFragment;
import com.farhad.sabtradecapital_deliveryman.ui.delivery.DeliveryReportFragment;
import com.farhad.sabtradecapital_deliveryman.viewmodels.SalesViewModel;

import java.util.ArrayList;
import java.util.List;

public class OrderListFragment extends Fragment {
    private Dialog loadingDialog;

    private RecyclerView rvOrderList;
    private ArrayList<OrderModel> orderLists;
    private OrderListAdapter salesListAdapter;

    private TextView totalAmountText;

    private SalesViewModel salesViewModel;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        salesViewModel = new ViewModelProvider(this).get(SalesViewModel.class);

        View root = inflater.inflate(R.layout.activity_order_list, container, false);
        getActivity().setTitle("All Order");

        initializeOrders(root);
        return root;
    }

    @Override
    public void onResume() {
        super.onResume();
        getActivity().setTitle("All Order");
    }

    private void initializeOrders(View view) {
        initializeDialog();

        rvOrderList = view.findViewById(R.id.activity_order_list_rv_details);
        totalAmountText = view.findViewById(R.id.order_list_total_text);
        orderLists = new ArrayList<>();

        salesViewModel.getAllSales().observe(getActivity(), new Observer<List<Sales>>() {
            @Override
            public void onChanged(List<Sales> salesList) {
                loadingDialog.dismiss();
                List<Sales> sales = new ArrayList<>();
                double total = 0.0;
                for(Sales s: salesList) {
                    if (s.getCustomer() != null) {
                        sales.add(s);
                        // calculate total
                        if(s.getTotalTp() != null){
                            total += Double.parseDouble(s.getTotalTp());
                        }
                    }


                }
                salesListAdapter = new OrderListAdapter(getContext(), sales);

                rvOrderList.setHasFixedSize(true);
                RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getContext());
                rvOrderList.setLayoutManager(layoutManager);
                rvOrderList.setAdapter(salesListAdapter);
                salesListAdapter.notifyDataSetChanged();

//                Toast.makeText(getContext(), "Total: " + salesList.size(), Toast.LENGTH_SHORT).show();
                /*for(Sales sale: salesList){
                    if(sale.getTotalTp() != null){
                        total += Double.parseDouble(sale.getTotalTp());
                    }
                }*/

                salesListAdapter.setOnItemClickListener(new OrderListAdapter.OnItemClickListener() {
                    @Override
                    public void onItemClick(Sales note) {
                        if(note.getDeliveryStatus().equals("delivered")){
                            Fragment fm = DeliveryReportFragment.newInstance(note.getId());
                            goToFragment(fm);
                        } else {
                            Fragment fm = CustomerDailyDeitalFragment.newInstance(note.getId());
                            goToFragment(fm);
                        }
                    }
                });

                totalAmountText.setText("Total: " + (float)total + " \u09F3");
            }
        });

    }

    private void initializeDialog() {
        loadingDialog = new Dialog(getContext());
        loadingDialog.setContentView(R.layout.loading_dialog);
        loadingDialog.getWindow().setLayout(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        loadingDialog.setCancelable(true);

        loadingDialog.show();
    }

    private void goToFragment(Fragment newFragment) {
        FragmentManager fm = getActivity().getSupportFragmentManager();
        FragmentTransaction transaction = fm.beginTransaction();
        transaction.replace(R.id.nav_host_fragment, newFragment);
        transaction.addToBackStack(null);
        transaction.commit();
//        fm.popBackStack();
    }
}
