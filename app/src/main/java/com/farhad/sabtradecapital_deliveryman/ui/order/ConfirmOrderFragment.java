package com.farhad.sabtradecapital_deliveryman.ui.order;

import android.app.Application;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.farhad.sabtradecapital_deliveryman.R;
import com.farhad.sabtradecapital_deliveryman.api.ApiClient;
import com.farhad.sabtradecapital_deliveryman.api.ApiServices;
import com.farhad.sabtradecapital_deliveryman.auth.User;
import com.farhad.sabtradecapital_deliveryman.db.DatabaseRef;
import com.farhad.sabtradecapital_deliveryman.models.Cart;
import com.farhad.sabtradecapital_deliveryman.models.Customer;
import com.farhad.sabtradecapital_deliveryman.models.delivery.DeliveryData;
import com.farhad.sabtradecapital_deliveryman.models.delivery.DeliveryResponse;
import com.farhad.sabtradecapital_deliveryman.models.delivery.DiscountData;
import com.farhad.sabtradecapital_deliveryman.models.delivery.DiscountModel;
import com.farhad.sabtradecapital_deliveryman.models.delivery.ReturnItem;
import com.farhad.sabtradecapital_deliveryman.models.invoice.Invoice;
import com.farhad.sabtradecapital_deliveryman.ui.customer.CustomerDailyDeitalFragment;
import com.farhad.sabtradecapital_deliveryman.viewmodels.CartViewModel;
import com.farhad.sabtradecapital_deliveryman.viewmodels.CustomerViewModel;
import com.farhad.sabtradecapital_deliveryman.viewmodels.DeliveryViewModel;
import com.farhad.sabtradecapital_deliveryman.viewmodels.InvoiceViewModel;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.farhad.sabtradecapital_deliveryman.LoginActivity.SHARED_PREFS;

public class ConfirmOrderFragment extends Fragment {

    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    private String mParam1;
    private int salesId;
    private double totalPrice;

    private ApiServices apiServices;


    public static ConfirmOrderFragment newInstance(int param1, double param2) {
        ConfirmOrderFragment fragment = new ConfirmOrderFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_PARAM1, param1);
        args.putDouble(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if(getArguments() != null){
            salesId = getArguments().getInt(ARG_PARAM1, 0);
            totalPrice = getArguments().getDouble(ARG_PARAM2, 0.0);
        }
    }

    public ConfirmOrderFragment() {
        // Required empty public constructor
    }
    // viewmodels
    private InvoiceViewModel invoiceViewModel;
    private CustomerViewModel customerViewModel;
    private CartViewModel cartViewModel;
    private DeliveryViewModel deliveryViewModel;

    private Button confirmOrderBtn, goBackBtn;
    private EditText edtPaidAmount, edtDiscountAmount;
    private TextView totalAmount, previousDue, payableAmount;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_confirm_order, container, false);
        getActivity().setTitle("Confirm Order");

        apiServices = ApiClient.getInstance(new User(getContext()).getName()).create(ApiServices.class);

        invoiceViewModel = new ViewModelProvider(getActivity()).get(InvoiceViewModel.class);
        cartViewModel = new ViewModelProvider(getActivity()).get(CartViewModel.class);
        customerViewModel = new ViewModelProvider(getActivity()).get(CustomerViewModel.class);
        deliveryViewModel = new ViewModelProvider(getActivity()).get(DeliveryViewModel.class);

        confirmOrderBtn = view.findViewById(R.id.confirm_order_btn);

        initializeViews(view);
        
        confirmOrderBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                confirmOrder();
//                Toast.makeText(getContext(), "Payment Successfylly Done", Toast.LENGTH_SHORT).show();
            }
        });
        
        return view;
    }

    private void confirmOrder() {
        List<ReturnItem> items = new ArrayList<>();
        confirmOrderBtn.setEnabled(false);
        DatabaseRef.getInstance(getContext()).dao().getCarts().observe(getViewLifecycleOwner(), new Observer<List<Cart>>() {
            @Override
            public void onChanged(List<Cart> carts) {
                if(!carts.isEmpty()){
                    for(Cart cart: carts){
                        items.add(new ReturnItem(cart.getProduct_id(), cart.getQuantity()));
                    }
                }
                DeliveryData deliveryData = new DeliveryData();
                deliveryData.setPayment(edtPaidAmount.getText().toString());
                deliveryData.setReturnItems(items);

                confirm(deliveryData);
            }
        });
        /*cartViewModel.getAllCarts().observe(getViewLifecycleOwner(), new Observer<List<Cart>>() {
            @Override
            public void onChanged(List<Cart> carts) {
                if(!carts.isEmpty()){
                    for(Cart cart: carts){
                        items.add(new ReturnItem(cart.getProduct_id(), cart.getQuantity()));
                    }
                }
                DeliveryData deliveryData = new DeliveryData();
                deliveryData.setPayment(edtPaidAmount.getText().toString());
                deliveryData.setReturnItems(items);

                confirm(deliveryData);

                //Log.d("TAG", "confirmOrder: " + deliveryData.getPayment());
                //Log.d("TAG", "confirmOrder: " + deliveryData.getReturnItems().size());
                //Log.d("TAG", "confirmOrder: " + deliveryData.getReturnItems().get(0).getReturn());
            }
        });*/



        /*DeliveryData deliveryData = new DeliveryData();
        deliveryData.setPayment(edtPaidAmount.getText().toString());
        deliveryData.setReturnItems(items);

        Log.d("TAG", "confirmOrder: " + deliveryData.getPayment());
        Log.d("TAG", "confirmOrder: " + deliveryData.getReturnItems().size());
        Log.d("TAG", "confirmOrder: " + deliveryData.getReturnItems().get(0).getReturn());*/

        /*
        deliveryViewModel.confirmDelivery(salesId, deliveryData).observe(getViewLifecycleOwner(), new Observer<DeliveryResponse>() {
            @Override
            public void onChanged(DeliveryResponse deliveryResponse) {
                showInputDialog(edtPaidAmount.getText().toString(), deliveryResponse.getMessage());
                cartViewModel.deleteAllCart();
            }
        });*/
    }

    private void confirm(DeliveryData deliveryData) {
        /*Toast.makeText(getContext(), "" + deliveryData.getReturnItems().size(), Toast.LENGTH_SHORT).show();
        Log.d("TAG", "confirm: " + deliveryData.getReturnItems().size());*/
        String discountAmount = edtDiscountAmount.getText().toString();
        if(!edtDiscountAmount.getText().toString().equals("")){
            apiServices.discount(salesId, new DiscountModel(discountAmount)).enqueue(new Callback<DiscountData>() {
                @Override
                public void onResponse(Call<DiscountData> call, Response<DiscountData> response) {
                    if(response.isSuccessful() && response.body().getHttpStatus() == 200){
                        Log.d("TAG", "onResponse: " + response.body().getMessage());
                    }
                }

                @Override
                public void onFailure(Call<DiscountData> call, Throwable t) {
                    Log.d("TAG", "onFailure: add discount " + t.toString());
                }
            });
        }

        apiServices.confirmDelivery(salesId, deliveryData).enqueue(new Callback<DeliveryResponse>() {
            @Override
            public void onResponse(Call<DeliveryResponse> call, Response<DeliveryResponse> response) {
                if(response.isSuccessful() && response.body().getHttpStatus() == 200){
                    showInputDialog(edtPaidAmount.getText().toString(), response.body().getMessage());
                    cartViewModel.deleteAllCart();
                } else {
                    Log.d("TAG", "onResponse: " + response.body().getMessage());
                    Toast.makeText(getContext(), "" + response.body().getMessage(), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<DeliveryResponse> call, Throwable t) {
                Log.d("TAG", "onFailure: " + t.toString());
                Toast.makeText(getContext(), "" + t.toString(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void initializeViews(View view) {
        totalAmount = view.findViewById(R.id.confirm_total_amount);
        previousDue = view.findViewById(R.id.confirm_previous_due);
        payableAmount = view.findViewById(R.id.confirm_payable_amount);
        edtPaidAmount = view.findViewById(R.id.confirm_edt_paid_amount);
        edtDiscountAmount = view.findViewById(R.id.edtDiscountAmount);

        invoiceViewModel.getSales(salesId).observe(getViewLifecycleOwner(), new Observer<Invoice>() {
            @Override
            public void onChanged(Invoice invoice) {
                Double total = totalPrice;
                totalAmount.setText("" + total.floatValue() + " \u09F3");
                Double prevDue = Double.parseDouble(invoice.getCustomer().getDue());
                previousDue.setText("" + prevDue.floatValue() + " \u09F3");

                Double payable = totalPrice + Double.parseDouble(invoice.getCustomer().getDue());
                payableAmount.setText(payable.floatValue() + " \u09F3\"");
            }
        });
    }

    private void showInputDialog(String paidAmount, String message) {
        final Dialog signInDialog = new Dialog(getContext());
        signInDialog.setContentView(R.layout.delivery_confirmation_dialog);
        signInDialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        signInDialog.setCancelable(true);
        final Button dialogOK = signInDialog.findViewById(R.id.delivery_confirmation_btn);
        final TextView paid = signInDialog.findViewById(R.id.paid_amount_dialog_text);
        final TextView messageText = signInDialog.findViewById(R.id.confirm_message);
        paid.setText(paidAmount + " \u09F3");
        messageText.setText(message);

        dialogOK.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Fragment newFragment = new OrderListFragment();
//                FragmentManager fm = getActivity().getSupportFragmentManager();
                FragmentTransaction transaction = getActivity().getSupportFragmentManager().beginTransaction();


                transaction.replace(R.id.nav_host_fragment, newFragment);
                transaction.addToBackStack(null);
                transaction.commit();
                signInDialog.dismiss();

            }
        });

        signInDialog.show();
    }

    public String loadData() {
        SharedPreferences sharedPreferences = getActivity().getSharedPreferences(SHARED_PREFS, Context.MODE_PRIVATE);
        String value = sharedPreferences.getString("ACCESS_KEY", "");
        return value;
    }
}