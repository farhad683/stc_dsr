package com.farhad.sabtradecapital_deliveryman.ui.customer;
import android.app.Application;
import android.app.Dialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.farhad.sabtradecapital_deliveryman.R;
import com.farhad.sabtradecapital_deliveryman.adapter.AreaSpinnerAdapter;
import com.farhad.sabtradecapital_deliveryman.adapter.CustomerListAdapter;
import com.farhad.sabtradecapital_deliveryman.adapter.SubZoneSpinnerAdapter;
import com.farhad.sabtradecapital_deliveryman.adapter.ZoneSpinnerAdapter;
import com.farhad.sabtradecapital_deliveryman.api.ApiClient;
import com.farhad.sabtradecapital_deliveryman.api.ApiServices;
import com.farhad.sabtradecapital_deliveryman.auth.User;
import com.farhad.sabtradecapital_deliveryman.customer.CustomerItem;
import com.farhad.sabtradecapital_deliveryman.models.area.Area;
import com.farhad.sabtradecapital_deliveryman.models.area_customer.Data;
import com.farhad.sabtradecapital_deliveryman.models.sales_create.SalesCreate;
import com.farhad.sabtradecapital_deliveryman.models.sales_order.Customer;
import com.farhad.sabtradecapital_deliveryman.models.sales_order.CustomerFromZone;
import com.farhad.sabtradecapital_deliveryman.models.subzone.SubZoneData;
import com.farhad.sabtradecapital_deliveryman.models.zone.Zone;
import com.farhad.sabtradecapital_deliveryman.viewmodels.AreaViewModel;
import com.farhad.sabtradecapital_deliveryman.viewmodels.CustomerViewModel;
import com.farhad.sabtradecapital_deliveryman.viewmodels.SalesViewModel;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.farhad.sabtradecapital_deliveryman.LoginActivity.SHARED_PREFS;

public class CustomerFragment extends Fragment {
    private Dialog loadingDialog;
    public String[] areaList = {"Dhaka", "Tangail", "Mirpur"};
    public String[] zoneList = {"zone 1", "zone 2", "zone 3"};
    public String[] filterList = {"filter 1", "filter 2", "filter 3"};

    private AreaViewModel areaViewModel;

    private Spinner spFilter, spArea, spZone, spSubZone;
    private RecyclerView rvCustomerList;
    private Button customer_layout_btn_total;
    private FloatingActionButton fabAddCustBtn;

    private ArrayAdapter<Data> areaListAdapter;
    private List<Area> areas;
    private ArrayAdapter<Zone> zoneListAdapter;

    private ZoneSpinnerAdapter zoneSpinnerAdapter;
    private AreaSpinnerAdapter areaSpinnerAdapter;

    private ArrayList<CustomerItem> mCustomerItemList;
    private CustomerListAdapter customerListAdapter;
    private CustomerViewModel customerViewModel;
    private SalesViewModel salesViewModel;

    private ApiServices apiServices;

    String[] filter;
    String[] area;
    String[] zone;

    private int areaId;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.activity_customer_list, container, false);
        customerViewModel = new ViewModelProvider(this).get(CustomerViewModel.class);
        areaViewModel = new ViewModelProvider(getActivity()).get(AreaViewModel.class);
        salesViewModel = new ViewModelProvider(getActivity()).get(SalesViewModel.class);

        apiServices = ApiClient.getInstance(new User(getContext()).getName()).create(ApiServices.class);

        filter = getResources().getStringArray(R.array.filter_list);
        area = getResources().getStringArray(R.array.area_list);
        zone = getResources().getStringArray(R.array.zone_list);


        initializeDialog();
        initializeCustomers(root);
        initializeFilters(root);


        customer_layout_btn_total = root.findViewById(R.id.customer_layout_btn_total);

        fabAddCustBtn = root.findViewById(R.id.fabAddCustBtn);
        fabAddCustBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Fragment fm = AddCustomerFragment.newInstance("fromCustomer");
                goToFragment(fm);
            }
        });
        return root;
    }

    private void goToFragment(Fragment newFragment) {
        FragmentTransaction transaction = getFragmentManager().beginTransaction();
        transaction.replace(R.id.nav_host_fragment, newFragment);
        transaction.addToBackStack(null);
        transaction.commit();
    }

    private void initializeFilters(View v) {
        spFilter = v.findViewById(R.id.customer_list_sp_filter);
        spArea = v.findViewById(R.id.customer_list_sp_area);
        spSubZone = v.findViewById(R.id.customer_sp_subzone);

        setArea();
        spZone = v.findViewById(R.id.customer_layout_sp_zone);

    }

    private void setArea() {
        apiServices.getAreasForSelect().enqueue(new Callback<com.farhad.sabtradecapital_deliveryman.models.area_customer.Area>() {
            @Override
            public void onResponse(Call<com.farhad.sabtradecapital_deliveryman.models.area_customer.Area> call, Response<com.farhad.sabtradecapital_deliveryman.models.area_customer.Area> response) {
                if (response.isSuccessful() && response.body().getHttpStatus() == 200) {

                    areaSpinnerAdapter = new AreaSpinnerAdapter(getActivity(), response.body().getData());
//                areaListAdapter = new ArrayAdapter<Data>(getActivity(), android.R.layout.simple_dropdown_item_1line, data);
                    spArea.setAdapter(areaSpinnerAdapter);
                    areaId = response.body().getData().get(0).getId();

                    spArea.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                        @Override
                        public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                            areaId = response.body().getData().get(position).getId();
//                        Toast.makeText(getContext(), "" + area_id, Toast.LENGTH_SHORT).show();
                            setZone();
                        }

                        @Override
                        public void onNothingSelected(AdapterView<?> parent) {

                        }
                    });
                } else {
                    Log.d("TAG", "onResponse: area list is " + response.body().getMessage());
                }
            }

            @Override
            public void onFailure(Call<com.farhad.sabtradecapital_deliveryman.models.area_customer.Area> call, Throwable t) {
                Log.d("TAG", "onFailure: " + t.toString());
            }
        });


    }

    private void setZone(){
        areaViewModel.getZoneFromArea(areaId).observe(getActivity(), new Observer<List<com.farhad.sabtradecapital_deliveryman.models.zone.Zone>>() {
            @Override
            public void onChanged(List<Zone> zones) {
                if (getActivity() != null) {
                    zoneSpinnerAdapter = new ZoneSpinnerAdapter(getActivity(), zones);
//                    zoneListAdapter = new ArrayAdapter<Zone>(getActivity(), android.R.layout.simple_dropdown_item_1line, zones);
                    spZone.setAdapter(zoneSpinnerAdapter);

                    spZone.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                        @Override
                        public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                            setSubZone(zones.get(position).getId());
                        /*customerViewModel.getAllCustomersFromZone(areaId).observe(getActivity(), new Observer<List<com.farhad.sabtradecapital.models.sales_order.Customer>>() {
                            @Override
                            public void onChanged(List<com.farhad.sabtradecapital.models.sales_order.Customer> customers) {
                                setCustomers(customers);
                            }
                        });*/
                        }

                        @Override
                        public void onNothingSelected(AdapterView<?> parent) {

                        }
                    });
                }
            }
        });
    }

    private void setSubZone(Integer id) {
        apiServices.getSubZoneFromZone(id).enqueue(new Callback<SubZoneData>() {
            @Override
            public void onResponse(Call<SubZoneData> call, Response<SubZoneData> response) {
                if(response.isSuccessful() && response.body().getHttpStatus() == 200){
                    SubZoneSpinnerAdapter subZoneSpinnerAdapter = new SubZoneSpinnerAdapter(getContext(), response.body().getData().getSubZone());
                    spSubZone.setAdapter(subZoneSpinnerAdapter);
                    spSubZone.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                        @Override
                        public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                            getCustomers(response.body().getData().getSubZone().get(position).getId());
                        }

                        @Override
                        public void onNothingSelected(AdapterView<?> parent) {

                        }
                    });
                }
            }

            @Override
            public void onFailure(Call<SubZoneData> call, Throwable t) {

            }
        });
    }

    private void getCustomers(int id) {
        apiServices.getSubZoneCustomer(id).enqueue(new Callback<CustomerFromZone>() {
            @Override
            public void onResponse(Call<CustomerFromZone> call, Response<CustomerFromZone> response) {
                if(response.isSuccessful() && response.body().getHttpStatus() == 200){
                    Log.d("TAG", "onResponse: " + response.body().getData().getCustomers().size() + ", " + id);
                    setCustomers(response.body().getData().getCustomers());
                } else {
                    Log.d("TAG", "onResponse: " + response.body().getMessage() + " " + id);
                }
            }

            @Override
            public void onFailure(Call<CustomerFromZone> call, Throwable t) {
                Log.d("TAG", "onFailure: customer f: " + t.toString()  + " " + id);
            }
        });
    }


    private void initializeCustomers(View v) {
//        loadingDialog.show();
        rvCustomerList = v.findViewById(R.id.customer_list_rv_details);
        mCustomerItemList = new ArrayList<>();
    }

    private void setCustomers(List<Customer> customers) {
        customerListAdapter = new CustomerListAdapter(getContext(), customers);
        customer_layout_btn_total.setText("Total Customer: " + customers.size());

        rvCustomerList.setHasFixedSize(true);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getContext());
        rvCustomerList.setLayoutManager(layoutManager);
        rvCustomerList.setAdapter(customerListAdapter);
        customerListAdapter.notifyDataSetChanged();
    }

    private void initializeDialog() {
        loadingDialog = new Dialog(getContext());
        loadingDialog.setContentView(R.layout.loading_dialog);
        loadingDialog.getWindow().setLayout(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        loadingDialog.setCancelable(true);
    }

    public String loadData() {
        SharedPreferences sharedPreferences = getActivity().getSharedPreferences(SHARED_PREFS, Context.MODE_PRIVATE);
        String value = sharedPreferences.getString("ACCESS_KEY", "");
        return value;
    }
}