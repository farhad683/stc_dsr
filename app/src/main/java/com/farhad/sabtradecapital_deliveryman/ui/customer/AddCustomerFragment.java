package com.farhad.sabtradecapital_deliveryman.ui.customer;

import android.app.Dialog;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;

import com.farhad.sabtradecapital_deliveryman.HomeActivity;
import com.farhad.sabtradecapital_deliveryman.R;
import com.farhad.sabtradecapital_deliveryman.adapter.AreaSpinnerAdapter;
import com.farhad.sabtradecapital_deliveryman.adapter.ZoneSpinnerAdapter;
import com.farhad.sabtradecapital_deliveryman.models.Customer;
import com.farhad.sabtradecapital_deliveryman.models.area.Area;
import com.farhad.sabtradecapital_deliveryman.models.area_customer.Data;
import com.farhad.sabtradecapital_deliveryman.models.zone.Zone;
import com.farhad.sabtradecapital_deliveryman.ui.home.HomeFragment;
import com.farhad.sabtradecapital_deliveryman.ui.sales_order.SalesOrderFragment;
import com.farhad.sabtradecapital_deliveryman.viewmodels.AreaViewModel;
import com.farhad.sabtradecapital_deliveryman.viewmodels.CustomerViewModel;

import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link AddCustomerFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class AddCustomerFragment extends Fragment {

    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    public AddCustomerFragment() {
        // Required empty public constructor
    }

    private Dialog loadingDialog;

    // adapters
    private ArrayAdapter<Data> areaListAdapter;
    private List<Area> areas;
    private ArrayAdapter<Zone> zoneListAdapter;
    private AreaSpinnerAdapter areaSpinnerAdapter;
    private ZoneSpinnerAdapter zoneSpinnerAdapter;

    // Viewmodels
    private AreaViewModel areaViewModel;
    private CustomerViewModel customerViewModel;


    /// views
    private EditText edtName, edtShopName, edtPhone, edtAddress;
    private Spinner spArea, spZone;
    private Button btnSave;

    private int areaId, zoneId;


    // TODO: Rename and change types and number of parameters
    public static AddCustomerFragment newInstance(String param1) {
        AddCustomerFragment fragment = new AddCustomerFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        ((HomeActivity) getActivity()).getSupportActionBar().setTitle("Add Customer");
        View view = inflater.inflate(R.layout.fragment_add_customer, container, false);

        areaViewModel = new ViewModelProvider(getActivity()).get(AreaViewModel.class);
        customerViewModel = new ViewModelProvider(getActivity()).get(CustomerViewModel.class);

        initializeViews(view);



        return view;
    }

    private void initializeViews(View view) {
        edtName = view.findViewById(R.id.add_cust_name_edt);
        edtShopName = view.findViewById(R.id.add_cust_shop_name_edt);
        edtPhone = view.findViewById(R.id.add_cust_phone_edt);
        edtAddress = view.findViewById(R.id.add_cust_address_edt);
        spArea = view.findViewById(R.id.add_cust_area_sp);
        spZone = view.findViewById(R.id.add_cust_zone_sp);
        btnSave = view.findViewById(R.id.add_cust_save_btn);

        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addCustomer();
            }
        });

        setArea();
    }

    private void setArea() {
        areaViewModel.getAreasForSelect().observe(getActivity(), new Observer<List<Data>>() {
            @Override
            public void onChanged(List<Data> data) {
                if (getActivity() != null) {
                    //areaSpinnerAdapter = new AreaSpinnerAdapter(getActivity(), data);
//                    areaListAdapter = new ArrayAdapter<Data>(getActivity(), android.R.layout.simple_dropdown_item_1line, data);
                    spArea.setAdapter(areaSpinnerAdapter);
                    areaId = data.get(0).getId();

                    spArea.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                        @Override
                        public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                            areaId = data.get(position).getId();
//                        Toast.makeText(getContext(), "" + area_id, Toast.LENGTH_SHORT).show();
                            setZone();
                        }

                        @Override
                        public void onNothingSelected(AdapterView<?> parent) {

                        }
                    });
                }
            }
        });

    }

    private void setZone(){
        areaViewModel.getZoneFromArea(areaId).observe(getActivity(), new Observer<List<com.farhad.sabtradecapital_deliveryman.models.zone.Zone>>() {
            @Override
            public void onChanged(List<Zone> zones) {
                if (getActivity() != null) {
                    zoneSpinnerAdapter = new ZoneSpinnerAdapter(getActivity(), zones);
//                    zoneListAdapter = new ArrayAdapter<Zone>(getActivity(), android.R.layout.simple_dropdown_item_1line, zones);
                    spZone.setAdapter(zoneSpinnerAdapter);

                    spZone.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                        @Override
                        public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                            zoneId = zones.get(position).getId();
                        }

                        @Override
                        public void onNothingSelected(AdapterView<?> parent) {

                        }
                    });
                }
            }
        });
    }

    private void addCustomer(){
        String name = edtName.getText().toString().trim();
        String shop = edtShopName.getText().toString().trim();
        String phone = edtPhone.getText().toString().trim();
        String address = edtAddress.getText().toString().trim();

        if(name.isEmpty()){
            edtName.setError("Name must be filled");
            return;
        }
        if(shop.isEmpty()){
            edtShopName.setError("Shop name must be filled");
            return;
        }
        if(phone.isEmpty()){
            edtPhone.setError("Phone must be filled");
            return;
        }
        if(address.isEmpty()){
            edtAddress.setError("Address must be filled");
            return;
        }

//        Toast.makeText(getContext(), "" + areaId+ ", " + zoneId, Toast.LENGTH_SHORT).show();

        Customer customer = new Customer(""+ areaId, "" + zoneId, name, shop, phone, address);

        customerViewModel.addCustomer(customer).observe(this, new Observer<Customer>() {
            @Override
            public void onChanged(Customer customer) {
                Toast.makeText(getContext(), "Customer successfully added!", Toast.LENGTH_SHORT).show();
                if(mParam1.equals("fromSales")){
                    goToFragment(new SalesOrderFragment());
                    ((HomeActivity) getActivity()).getSupportActionBar().setTitle("Create Sales");
                } else if(mParam1.equals("fromCustomer")){
                    goToFragment(new CustomerFragment());
                    ((HomeActivity) getActivity()).getSupportActionBar().setTitle("Home");
                } else {
                    goToFragment(new HomeFragment());
                    ((HomeActivity) getActivity()).getSupportActionBar().setTitle("Home");
                }
            }
        });
    }

    private void initializeDialog() {
        loadingDialog = new Dialog(getContext());
        loadingDialog.setContentView(R.layout.loading_dialog);
        loadingDialog.getWindow().setLayout(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        loadingDialog.setCancelable(true);
    }

    private void goToFragment(Fragment newFragment) {
        FragmentTransaction transaction = getFragmentManager().beginTransaction();
        transaction.replace(R.id.nav_host_fragment, newFragment);
        transaction.addToBackStack(null);
        transaction.commit();
    }
}