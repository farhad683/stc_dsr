package com.farhad.sabtradecapital_deliveryman;

import androidx.appcompat.app.AppCompatActivity;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.farhad.sabtradecapital_deliveryman.api.ApiClient;
import com.farhad.sabtradecapital_deliveryman.api.ApiRef;
import com.farhad.sabtradecapital_deliveryman.api.ApiServices;
import com.farhad.sabtradecapital_deliveryman.models.Data;
import com.farhad.sabtradecapital_deliveryman.models.User;

import java.util.Date;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginActivity extends BaseActivity {
    public static final String SHARED_PREFS = "delivery_sharedPrefs";
    private Dialog loadingDialog;

    private EditText edtPhone, edtPassword;
    private Button btnLogin;

    private Date date;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        setTitle("");

        edtPhone = findViewById(R.id.login_layout_edt_mobile);
        edtPassword = findViewById(R.id.login_layout_edt_password);
        btnLogin = findViewById(R.id.login_layout_btn_login);
        initializeDialog();

        date  = new Date();

        com.farhad.sabtradecapital_deliveryman.auth.User user = new com.farhad.sabtradecapital_deliveryman.auth.User(this);
        if(user.getName() != "" && (user.getDate() + "").equals("" + date.getDate())){
            startActivity(new Intent(LoginActivity.this, HomeActivity.class));
//            startActivity(new Intent(LoginActivity.this, HomeActivity.class));
            finish();
        } else {
            new com.farhad.sabtradecapital_deliveryman.auth.User(this).removeUser();
        }

        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                loadingDialog.show();
                String phone = edtPhone.getText().toString();
                String pass = edtPassword.getText().toString();
                if(phone.isEmpty() || pass.isEmpty()){
                    Toast.makeText(LoginActivity.this, "Email or password can not be empty", Toast.LENGTH_SHORT).show();
                    loadingDialog.dismiss();
                } else {
                    ApiServices apiServices = ApiRef.getInstance().create(ApiServices.class);
                    apiServices.login(phone, pass).enqueue(new Callback<User>() {
                        @Override
                        public void onResponse(Call<User> call, Response<User> response) {
                            if(response.isSuccessful()){
                                if(response.code() == 200){
                                    saveData(response.body().getData().getAccessToken(), response.body().getData().getUser().getName(), response.body().getData().getUser().getEmail());
                                    startActivity(new Intent(LoginActivity.this, HomeActivity.class));

                                    user.setName(response.body().getData().getAccessToken());
                                    user.setDate("" + date.getDate());

                                    loadingDialog.dismiss();
                                    finish();
                                }
                                Log.d("TAG", "onResponse: " + response.body().getData().getAccessToken());
                            } else {
                                Toast.makeText(LoginActivity.this, "Invalid email or password", Toast.LENGTH_SHORT).show();
                            }
                        }

                        @Override
                        public void onFailure(Call<User> call, Throwable t) {
                            Log.d("TAG", "onResponse: " + t.getMessage());
                            Log.d("TAG", "onResponse: " + t.getMessage());
                            Toast.makeText(LoginActivity.this, "Invalid email or password", Toast.LENGTH_SHORT).show();
                            loadingDialog.dismiss();
                        }
                    });
//
                }
            }
        });

    }

    public void saveData(String accessKey, String user, String email) {
        SharedPreferences sharedPreferences = getSharedPreferences(SHARED_PREFS, MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString("ACCESS_KEY", accessKey);
        editor.putString("USERNAME", user);
        editor.putString("EMAIL", email);
        editor.putString("PASSWORD", edtPassword.getText().toString());
        editor.apply();
        Toast.makeText(this, "Login Successfull", Toast.LENGTH_SHORT).show();
    }

    public String loadData() {
        SharedPreferences sharedPreferences = getSharedPreferences(SHARED_PREFS, Context.MODE_PRIVATE);
        String value = sharedPreferences.getString("ACCESS_KEY", "");
        return value;
    }
    private void initializeDialog() {
        loadingDialog = new Dialog(this);
        loadingDialog.setContentView(R.layout.loading_dialog);
        loadingDialog.getWindow().setLayout(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        loadingDialog.setCancelable(true);
    }

}
