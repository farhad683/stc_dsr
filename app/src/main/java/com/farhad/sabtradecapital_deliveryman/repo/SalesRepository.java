package com.farhad.sabtradecapital_deliveryman.repo;

import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

import androidx.lifecycle.MutableLiveData;

import com.farhad.sabtradecapital_deliveryman.api.ApiClient;
import com.farhad.sabtradecapital_deliveryman.api.ApiServices;
import com.farhad.sabtradecapital_deliveryman.auth.User;
import com.farhad.sabtradecapital_deliveryman.models.sales.Data;
import com.farhad.sabtradecapital_deliveryman.models.sales.Sales;
import com.farhad.sabtradecapital_deliveryman.models.sales_create.SalesCreate;
import com.farhad.sabtradecapital_deliveryman.models.sales_order.CreateSales;
import com.farhad.sabtradecapital_deliveryman.models.sales_order.SalesResponse;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.farhad.sabtradecapital_deliveryman.LoginActivity.SHARED_PREFS;

public class SalesRepository {

    private ApiServices apiServices;
    private MutableLiveData<List<Sales>> allSales;
    private MutableLiveData<List<Sales>> todaysSales;
    private MutableLiveData<SalesResponse> salesResponse;
    private MutableLiveData<SalesCreate> salesCreate;
    private Application application;

    public SalesRepository(Application application) {
        this.application = application;
        this.apiServices = ApiClient.getInstance(new User(application).getName()).create(ApiServices.class);
        allSales = new MutableLiveData<>();
        todaysSales = new MutableLiveData<>();
        salesResponse = new MutableLiveData<>();
    }

    public MutableLiveData<SalesCreate> getSalesCreate(){
        apiServices.getSalesCreate().enqueue(new Callback<SalesCreate>() {
            @Override
            public void onResponse(Call<SalesCreate> call, Response<SalesCreate> response) {
                if(response.isSuccessful()){
                    salesCreate.setValue(response.body());
                }
            }

            @Override
            public void onFailure(Call<SalesCreate> call, Throwable t) {

            }
        });
        return salesCreate;
    }

    public MutableLiveData<List<Sales>> getSales(){
        apiServices.getSales().enqueue(new Callback<Data>() {
            @Override
            public void onResponse(Call<Data> call, Response<Data> response) {
                if(response.isSuccessful()){
                    allSales.setValue(response.body().getData());
                }
            }

            @Override
            public void onFailure(Call<Data> call, Throwable t) {

            }
        });
        return allSales;
    }
    public MutableLiveData<List<Sales>> getTodaysSales(){
        apiServices.getTodaysSales().enqueue(new Callback<Data>() {
            @Override
            public void onResponse(Call<Data> call, Response<Data> response) {
                if(response.isSuccessful()){
                    todaysSales.setValue(response.body().getData());
                }
            }

            @Override
            public void onFailure(Call<Data> call, Throwable t) {

            }
        });
        return todaysSales;
    }

    public MutableLiveData<SalesResponse> createSales(CreateSales createSales){
        apiServices.createSales(createSales).enqueue(new Callback<SalesResponse>() {
            @Override
            public void onResponse(Call<SalesResponse> call, Response<SalesResponse> response) {
                Log.d("TAG", "onResponse: " + response.code());
                if (response.isSuccessful()){
                    salesResponse.setValue(response.body());
                } else {
                    Log.d("TAG", "onResponse: " + response.code());
                    Log.d("TAG", "onResponse: " + response.message());
                }
            }

            @Override
            public void onFailure(Call<SalesResponse> call, Throwable t) {
                Log.d("TAG", "onFailure: " + t.getMessage());
            }
        });
        return salesResponse;
    }


    public String loadData(Application application) {
        SharedPreferences sharedPreferences = application.getSharedPreferences(SHARED_PREFS, Context.MODE_PRIVATE);
        String value = sharedPreferences.getString("ACCESS_KEY", "");
        return value;
    }


}
