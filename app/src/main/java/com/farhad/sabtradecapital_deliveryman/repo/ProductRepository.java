package com.farhad.sabtradecapital_deliveryman.repo;

import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

import androidx.lifecycle.MutableLiveData;

import com.farhad.sabtradecapital_deliveryman.api.ApiClient;
import com.farhad.sabtradecapital_deliveryman.api.ApiServices;
import com.farhad.sabtradecapital_deliveryman.auth.User;
import com.farhad.sabtradecapital_deliveryman.models.category.Category;
import com.farhad.sabtradecapital_deliveryman.models.category.CategoryData;
import com.farhad.sabtradecapital_deliveryman.models.product.Product;
import com.farhad.sabtradecapital_deliveryman.models.product.ProductData;
import com.farhad.sabtradecapital_deliveryman.models.stock.StockData;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.farhad.sabtradecapital_deliveryman.LoginActivity.SHARED_PREFS;

public class ProductRepository {
    private ApiServices apiServices;
    private MutableLiveData<List<Category>> allCategories;
    private MutableLiveData<List<Product>> productsFromCategory;
    private MutableLiveData<StockData> stockData;
    private Application application;

    public ProductRepository(Application application) {
        this.application = application;
        apiServices = ApiClient.getInstance(new User(application).getName()).create(ApiServices.class);
        allCategories = new MutableLiveData<>();
        productsFromCategory = new MutableLiveData<>();
        stockData = new MutableLiveData<>();
    }

    public MutableLiveData<List<Category>> getAllCategories(){
        apiServices.getCategories().enqueue(new Callback<CategoryData>() {
            @Override
            public void onResponse(Call<CategoryData> call, Response<CategoryData> response) {
                allCategories.setValue(response.body().getData());
            }

            @Override
            public void onFailure(Call<CategoryData> call, Throwable t) {

            }
        });
        return allCategories;
    }

    public MutableLiveData<List<Product>> getProductsFromCategory(int id){
        apiServices.getProductFromCategory(id).enqueue(new Callback<ProductData>() {
            @Override
            public void onResponse(Call<ProductData> call, Response<ProductData> response) {
                if(response.isSuccessful()){
                    productsFromCategory.setValue(response.body().getData().getProducts());
                    Log.d("TAG", "onResponse: " + response.code());
                }
            }

            @Override
            public void onFailure(Call<ProductData> call, Throwable t) {
                Log.d("TAG", "onFailure: " + t.getMessage());
            }
        });
        return productsFromCategory;
    }

    public MutableLiveData<StockData> getStocks(){
       apiServices.getStocks().enqueue(new Callback<StockData>() {
           @Override
           public void onResponse(Call<StockData> call, Response<StockData> response) {
               if(response.isSuccessful()){
                   stockData.setValue(response.body());
               } else {

                   Log.d("TAG", "onFailure: " + response.message());
               }
           }

           @Override
           public void onFailure(Call<StockData> call, Throwable t) {
               Log.d("TAG", "onFailure: " + t.getMessage());
           }
       });
        return stockData;
    }




    public String loadData(Application application) {
        SharedPreferences sharedPreferences = application.getSharedPreferences(SHARED_PREFS, Context.MODE_PRIVATE);
        String value = sharedPreferences.getString("ACCESS_KEY", "");
        return value;
    }

}
