package com.farhad.sabtradecapital_deliveryman.repo;

import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

import androidx.lifecycle.MutableLiveData;

import com.farhad.sabtradecapital_deliveryman.api.ApiClient;
import com.farhad.sabtradecapital_deliveryman.api.ApiServices;
import com.farhad.sabtradecapital_deliveryman.auth.User;
import com.farhad.sabtradecapital_deliveryman.models.customer.Customer;
import com.farhad.sabtradecapital_deliveryman.models.customer.CustomerData;
import com.farhad.sabtradecapital_deliveryman.models.customer.CustomerResponse;
import com.farhad.sabtradecapital_deliveryman.models.customer.Data;
import com.farhad.sabtradecapital_deliveryman.models.sales_order.CustomerFromZone;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.farhad.sabtradecapital_deliveryman.LoginActivity.SHARED_PREFS;

public class CustomerRepository {
    private ApiServices apiServices;
    private MutableLiveData<List<Customer>> allCustomers;
    private MutableLiveData<Data> customerData;
    private MutableLiveData<List<com.farhad.sabtradecapital_deliveryman.models.sales_order.Customer>> allCustomerFromZone;
    private MutableLiveData<com.farhad.sabtradecapital_deliveryman.models.Customer> addedCustomer;
    private Application application;

    public CustomerRepository(Application application) {
        this.application = application;
        this.apiServices = ApiClient.getInstance(new User(application).getName()).create(ApiServices.class);
        allCustomers = new MutableLiveData<>();
        addedCustomer = new MutableLiveData<>();
        allCustomerFromZone = new MutableLiveData<>();
        customerData = new MutableLiveData<>();
    }

    public MutableLiveData<Data> getCustomerData(int id){
        apiServices.getCustomer(id).enqueue(new Callback<CustomerResponse>() {
            @Override
            public void onResponse(Call<CustomerResponse> call, Response<CustomerResponse> response) {
                if(response.isSuccessful()){
                    if(response.body().getData() != null){
                        customerData.setValue(response.body().getData());
                    }
                } else {
                    Log.d("TAG", "onFailure: " + response.message());
                }
            }

            @Override
            public void onFailure(Call<CustomerResponse> call, Throwable t) {
                Log.d("TAG", "onFailure: " + t.getMessage());
            }
        });
        return customerData;
    }



    public MutableLiveData<List<Customer>> getAllCustomers(){
        apiServices.getCustomers().enqueue(new Callback<CustomerData>() {
            @Override
            public void onResponse(Call<CustomerData> call, Response<CustomerData> response) {
                if(response.isSuccessful() && response.body().getHttpStatus() == 200){
                    allCustomers.setValue(response.body().getData());
                } else {
                    Log.d("TAG", "onResponse: " + response.body().getMessage() + response.body().getHttpStatus());
                }
            }

            @Override
            public void onFailure(Call<CustomerData> call, Throwable t) {
                Log.d("TAG", "onFailure: " + t.toString());
            }
        });
        return allCustomers;
    }

    public MutableLiveData<List<com.farhad.sabtradecapital_deliveryman.models.sales_order.Customer>> getAllCustomerFromZone(int id){
        apiServices.getCustomersFromZone(id).enqueue(new Callback<CustomerFromZone>() {
            @Override
            public void onResponse(Call<CustomerFromZone> call, Response<CustomerFromZone> response) {
                if(response.isSuccessful()){
                    if(!response.body().getData().getCustomers().isEmpty()) {
                        allCustomerFromZone.setValue(response.body().getData().getCustomers());
                    }
                } else {
                    Log.d("TAG", "onResponse: " + response.message());
                }
            }

            @Override
            public void onFailure(Call<CustomerFromZone> call, Throwable t) {
                Log.d("TAG", "onResponse: " + t.getMessage());
            }
        });
        return allCustomerFromZone;
    }

    public MutableLiveData<com.farhad.sabtradecapital_deliveryman.models.Customer> addCustomer(com.farhad.sabtradecapital_deliveryman.models.Customer customer){
        apiServices.addCustomer(customer).enqueue(new Callback<com.farhad.sabtradecapital_deliveryman.models.Customer>() {
            @Override
            public void onResponse(Call<com.farhad.sabtradecapital_deliveryman.models.Customer> call, Response<com.farhad.sabtradecapital_deliveryman.models.Customer> response) {
                if(response.isSuccessful()){
                    addedCustomer.setValue(response.body());
                }

            }

            @Override
            public void onFailure(Call<com.farhad.sabtradecapital_deliveryman.models.Customer> call, Throwable t) {

            }
        });
        return addedCustomer;
    }

    public String loadData(Application application) {
        SharedPreferences sharedPreferences = application.getSharedPreferences(SHARED_PREFS, Context.MODE_PRIVATE);
        String value = sharedPreferences.getString("ACCESS_KEY", "");
        return value;
    }


}
