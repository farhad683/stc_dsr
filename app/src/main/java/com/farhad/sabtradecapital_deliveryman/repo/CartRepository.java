package com.farhad.sabtradecapital_deliveryman.repo;

import android.app.Application;
import android.os.AsyncTask;

import androidx.lifecycle.LiveData;

import com.farhad.sabtradecapital_deliveryman.db.DatabaseRef;
import com.farhad.sabtradecapital_deliveryman.db.SabtradeDao;
import com.farhad.sabtradecapital_deliveryman.models.Cart;

import java.util.List;

public class CartRepository {
    private SabtradeDao dao;
    private LiveData<List<Cart>> allCarts;

    public CartRepository(Application application) {
        DatabaseRef databaseRef = DatabaseRef.getInstance(application);
        dao = databaseRef.dao();
        allCarts = dao.getCarts();
    }

    public void insertCart(Cart cart){
        new InsertCartAsyncTask(dao).execute(cart);
    }

    public void updateCart(Cart cart){
        new UpdateCartAsyncTask(dao).execute(cart);
    }

    public void deleteCart(Cart cart){
        new DeleteCartAsyncTask(dao).execute(cart);
    }

    public void deleteAllCart(){
        new DeleteAllCartAsyncTask(dao).execute();
    }

    public LiveData<List<Cart>> getAllCarts(){
        return allCarts;
    }

    private static class InsertCartAsyncTask extends AsyncTask<Cart, Void, Void>{
        private SabtradeDao dao;

        public InsertCartAsyncTask(SabtradeDao dao) {
            this.dao = dao;
        }

        @Override
        protected Void doInBackground(Cart... carts) {
            dao.insert(carts[0]);
            return null;
        }
    }

    private static class UpdateCartAsyncTask extends AsyncTask<Cart, Void, Void>{
        private SabtradeDao dao;

        public UpdateCartAsyncTask(SabtradeDao dao) {
            this.dao = dao;
        }

        @Override
        protected Void doInBackground(Cart... carts) {
            dao.updateCart(carts[0]);
            return null;
        }
    }

    private static class DeleteCartAsyncTask extends AsyncTask<Cart, Void, Void>{
        private SabtradeDao dao;

        public DeleteCartAsyncTask(SabtradeDao dao) {
            this.dao = dao;
        }

        @Override
        protected Void doInBackground(Cart... carts) {
            dao.deleteCart(carts[0]);
            return null;
        }
    }

    private static class DeleteAllCartAsyncTask extends AsyncTask<Void, Void, Void>{
        private SabtradeDao dao;

        public DeleteAllCartAsyncTask(SabtradeDao dao) {
            this.dao = dao;
        }

        @Override
        protected Void doInBackground(Void... voids) {
            dao.deleteAllCart();
            return null;
        }
    }
}
