package com.farhad.sabtradecapital_deliveryman.repo;

import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

import androidx.lifecycle.MutableLiveData;

import com.farhad.sabtradecapital_deliveryman.api.ApiClient;
import com.farhad.sabtradecapital_deliveryman.api.ApiServices;
import com.farhad.sabtradecapital_deliveryman.models.invoice.Data;
import com.farhad.sabtradecapital_deliveryman.models.invoice.Invoice;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.farhad.sabtradecapital_deliveryman.LoginActivity.SHARED_PREFS;

public class InvoiceRepository {
    private ApiServices apiServices;
    private MutableLiveData<Invoice> invoice;
    private Application application;

    public InvoiceRepository(Application application) {
        this.application = application;
        this.apiServices = ApiClient.getInstance(loadData(application)).create(ApiServices.class);
        invoice = new MutableLiveData<>();
    }



    public MutableLiveData<Invoice> getSales(int id){
        apiServices.getSales(id).enqueue(new Callback<Data>() {
            @Override
            public void onResponse(Call<Data> call, Response<Data> response) {
                if(response.isSuccessful() && response.body().getHttpStatus() == 200){
                    invoice.setValue(response.body().getData());
                } else {
                    Log.d("TAG", "onResponse: " + response.body().getMessage());
                }
            }

            @Override
            public void onFailure(Call<Data> call, Throwable t) {
                Log.d("TAG", "onFailure: " + t.toString());
            }
        });
        return invoice;
    }



    public String loadData(Application application) {
        SharedPreferences sharedPreferences = application.getSharedPreferences(SHARED_PREFS, Context.MODE_PRIVATE);
        String value = sharedPreferences.getString("ACCESS_KEY", "");
        return value;
    }


}
