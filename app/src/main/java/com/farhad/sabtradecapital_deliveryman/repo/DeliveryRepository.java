package com.farhad.sabtradecapital_deliveryman.repo;

import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

import androidx.lifecycle.MutableLiveData;

import com.farhad.sabtradecapital_deliveryman.api.ApiClient;
import com.farhad.sabtradecapital_deliveryman.api.ApiServices;
import com.farhad.sabtradecapital_deliveryman.auth.User;
import com.farhad.sabtradecapital_deliveryman.models.delivery.DeliveryData;
import com.farhad.sabtradecapital_deliveryman.models.delivery.DeliveryResponse;
import com.farhad.sabtradecapital_deliveryman.models.invoice.Data;
import com.farhad.sabtradecapital_deliveryman.models.invoice.Invoice;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.farhad.sabtradecapital_deliveryman.LoginActivity.SHARED_PREFS;

public class DeliveryRepository {
    private ApiServices apiServices;
    private MutableLiveData<DeliveryResponse> deliveryResponse;
    private Application application;

    public DeliveryRepository(Application application) {
        this.application = application;
        this.apiServices = ApiClient.getInstance(new User(application).getName()).create(ApiServices.class);
        deliveryResponse = new MutableLiveData<>();
    }



    public MutableLiveData<DeliveryResponse> confirmDelivery(int id, DeliveryData deliveryData){
        apiServices.confirmDelivery(id, deliveryData).enqueue(new Callback<DeliveryResponse>() {
            @Override
            public void onResponse(Call<DeliveryResponse> call, Response<DeliveryResponse> response) {
                if(response.isSuccessful()){
                    deliveryResponse.setValue(response.body());
                } else {
                    Log.d("TAG", "onResponse: " + response.message());
                }
            }

            @Override
            public void onFailure(Call<DeliveryResponse> call, Throwable t) {

            }
        });
        return deliveryResponse;
    }



    public String loadData(Application application) {
        SharedPreferences sharedPreferences = application.getSharedPreferences(SHARED_PREFS, Context.MODE_PRIVATE);
        String value = sharedPreferences.getString("ACCESS_KEY", "");
        return value;
    }


}
