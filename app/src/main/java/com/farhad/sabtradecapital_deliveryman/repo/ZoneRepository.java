package com.farhad.sabtradecapital_deliveryman.repo;

import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;

import androidx.lifecycle.MutableLiveData;

import com.farhad.sabtradecapital_deliveryman.api.ApiClient;
import com.farhad.sabtradecapital_deliveryman.api.ApiServices;
import com.farhad.sabtradecapital_deliveryman.auth.User;
import com.farhad.sabtradecapital_deliveryman.models.zone.Zone;
import com.farhad.sabtradecapital_deliveryman.models.zone.ZoneData;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.farhad.sabtradecapital_deliveryman.LoginActivity.SHARED_PREFS;

public class ZoneRepository {
    private ApiServices apiServices;
    private MutableLiveData<List<Zone>> allZonesFromArea;
    private Application application;

    public ZoneRepository(Application application) {
        this.application = application;
        apiServices = ApiClient.getInstance(new User(application).getName()).create(ApiServices.class);
        allZonesFromArea = new MutableLiveData<>();
    }

    public MutableLiveData<List<Zone>> getAllZoneFromArea(int id){
        apiServices.getZoneFromArea(id).enqueue(new Callback<ZoneData>() {
            @Override
            public void onResponse(Call<ZoneData> call, Response<ZoneData> response) {
                if (response.isSuccessful()){
                    try {
                        allZonesFromArea.setValue(response.body().getData().getZones());
                    } catch (Exception e){

                    }
                }
            }

            @Override
            public void onFailure(Call<ZoneData> call, Throwable t) {

            }
        });
        return allZonesFromArea;
    }

    public String loadData(Application application) {
        SharedPreferences sharedPreferences = application.getSharedPreferences(SHARED_PREFS, Context.MODE_PRIVATE);
        String value = sharedPreferences.getString("ACCESS_KEY", "");
        return value;
    }


}
