package com.farhad.sabtradecapital_deliveryman.repo;

import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;

import androidx.lifecycle.MutableLiveData;

import com.farhad.sabtradecapital_deliveryman.api.ApiClient;
import com.farhad.sabtradecapital_deliveryman.api.ApiServices;
import com.farhad.sabtradecapital_deliveryman.auth.User;
import com.farhad.sabtradecapital_deliveryman.models.area.Area;
import com.farhad.sabtradecapital_deliveryman.models.area.AreaData;
import com.farhad.sabtradecapital_deliveryman.models.area_customer.Data;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.farhad.sabtradecapital_deliveryman.LoginActivity.SHARED_PREFS;

public class AreaRepository {
    private ApiServices apiServices;
    private MutableLiveData<List<Area>> allAreas;
    private MutableLiveData<List<Data>> allAreasForSelect;
    private Application application;

    public AreaRepository(Application application) {
        this.application = application;
        apiServices = ApiClient.getInstance(new User(application).getName()).create(ApiServices.class);
        allAreas = new MutableLiveData<>();
        allAreasForSelect = new MutableLiveData<>();
    }

    public MutableLiveData<List<Area>> getAllAreas(){
        apiServices.getAreas().enqueue(new Callback<AreaData>() {
            @Override
            public void onResponse(Call<AreaData> call, Response<AreaData> response) {
                if(response.isSuccessful()){
                    allAreas.setValue(response.body().getData());
                }
            }

            @Override
            public void onFailure(Call<AreaData> call, Throwable t) {

            }
        });
        return allAreas;
    }
    public MutableLiveData<List<Data>> getAreasForSelect(){
        apiServices.getAreasForSelect().enqueue(new Callback<com.farhad.sabtradecapital_deliveryman.models.area_customer.Area>() {
            @Override
            public void onResponse(Call<com.farhad.sabtradecapital_deliveryman.models.area_customer.Area> call, Response<com.farhad.sabtradecapital_deliveryman.models.area_customer.Area> response) {
                if(response.isSuccessful()){
                    try {
                        allAreasForSelect.setValue(response.body().getData());
                    } catch (Exception e){

                    }

                }
            }

            @Override
            public void onFailure(Call<com.farhad.sabtradecapital_deliveryman.models.area_customer.Area> call, Throwable t) {

            }
        });
        return allAreasForSelect;
    }



    public String loadData(Application application) {
        SharedPreferences sharedPreferences = application.getSharedPreferences(SHARED_PREFS, Context.MODE_PRIVATE);
        String value = sharedPreferences.getString("ACCESS_KEY", "");
        return value;
    }

}
