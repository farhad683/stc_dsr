package com.farhad.sabtradecapital_deliveryman;

import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.farhad.sabtradecapital_deliveryman.auth.LogoutListener;
import com.farhad.sabtradecapital_deliveryman.auth.MyApp;
import com.farhad.sabtradecapital_deliveryman.auth.User;


public class BaseActivity extends AppCompatActivity implements LogoutListener {
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        ((MyApp) getApplication()).registerSessionListener(this);
        ((MyApp) getApplication()).startUserSession();
    }

    @Override
    public void onUserInteraction() {
        super.onUserInteraction();
        ((MyApp) getApplication()).onUserInteracted();
    }

    @Override
    public void onSessionLogout() {
        finish();
        new User(this).removeUser();
        startActivity(new Intent(this, LoginActivity.class));
    }
}
